-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 14 авг 2019 в 13:01
-- Версия на сървъра: 10.2.26-MariaDB-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myqaspac_virtual_teacher`
--

-- --------------------------------------------------------

--
-- Структура на таблица `assignments`
--

CREATE TABLE `assignments` (
  `id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file` longblob NOT NULL,
  `file_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `authorities`
--

CREATE TABLE `authorities` (
  `username` varchar(15) NOT NULL,
  `authority` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `image` longblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `program` text DEFAULT NULL,
  `teacher` varchar(15) NOT NULL,
  `avg_rating` double DEFAULT 0,
  `status_id` int(11) NOT NULL DEFAULT 2,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `course_images`
--

CREATE TABLE `course_images` (
  `id` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `image` longblob NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `course_ratings`
--

CREATE TABLE `course_ratings` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `username` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `course_statuses`
--

CREATE TABLE `course_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `course_statuses`
--

INSERT INTO `course_statuses` (`id`, `name`) VALUES
(1, 'Active'),
(3, 'Deleted'),
(2, 'Draft');

-- --------------------------------------------------------

--
-- Структура на таблица `homeworks`
--

CREATE TABLE `homeworks` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `file` longblob DEFAULT NULL,
  `file_name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `grade` double DEFAULT NULL,
  `lecture_id` int(11) NOT NULL,
  `student` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `lectures`
--

CREATE TABLE `lectures` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `prev_lecture_id` int(11) DEFAULT NULL,
  `next_lecture_id` int(11) DEFAULT NULL,
  `videoURL` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `pending_approvals`
--

CREATE TABLE `pending_approvals` (
  `id` int(11) NOT NULL,
  `resume` varchar(500) NOT NULL,
  `username` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `middle_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `password` varchar(64) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `user_courses`
--

CREATE TABLE `user_courses` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `is_completed` bit(1) NOT NULL,
  `student` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `user_images`
--

CREATE TABLE `user_images` (
  `id` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `image` longblob DEFAULT NULL,
  `username` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `user_lectures`
--

CREATE TABLE `user_lectures` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  `student` varchar(15) NOT NULL,
  `video_watched` bit(1) NOT NULL DEFAULT b'0',
  `homework_submitted` bit(1) NOT NULL DEFAULT b'0',
  `available` bit(1) NOT NULL DEFAULT b'0',
  `prev_lecture_id` int(11) DEFAULT NULL,
  `next_lecture_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignments_lectures_id_fk` (`lecture_id`);

--
-- Indexes for table `authorities`
--
ALTER TABLE `authorities`
  ADD KEY `authorities_users_username_fk` (`username`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_uindex` (`name`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `courses_title_uindex` (`title`),
  ADD KEY `courses_users_username_fk` (`teacher`),
  ADD KEY `FK72l5dj585nq7i6xxv1vj51lyn` (`category_id`),
  ADD KEY `courses_course_statuses_id_fk` (`status_id`),
  ADD KEY `courses_course_images_id_fk` (`image_id`);

--
-- Indexes for table `course_images`
--
ALTER TABLE `course_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_ratings`
--
ALTER TABLE `course_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_ratings_courses_id_fk` (`course_id`),
  ADD KEY `course_ratings_users_username_fk` (`username`);

--
-- Indexes for table `course_statuses`
--
ALTER TABLE `course_statuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `course_statuses_name_uindex` (`name`);

--
-- Indexes for table `homeworks`
--
ALTER TABLE `homeworks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homeworks_courses_id_fk` (`course_id`),
  ADD KEY `homeworks_lectures_id_fk` (`lecture_id`),
  ADD KEY `homeworks_users_username_fk` (`student`);

--
-- Indexes for table `lectures`
--
ALTER TABLE `lectures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lectures_assignmets_assignment_id_fk` (`assignment_id`),
  ADD KEY `lectures_lectures_next_lecture_id_fk` (`next_lecture_id`),
  ADD KEY `lectures_courses_course_id_fk` (`course_id`),
  ADD KEY `lectures_lectures_id_fk` (`prev_lecture_id`);

--
-- Indexes for table `pending_approvals`
--
ALTER TABLE `pending_approvals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pending_approvals_users_username_fk` (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_uindex` (`username`),
  ADD KEY `users_user_images_id_fk` (`image_id`);

--
-- Indexes for table `user_courses`
--
ALTER TABLE `user_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_courses_courses_id_fk` (`course_id`),
  ADD KEY `user_courses_users_username_fk` (`student`);

--
-- Indexes for table `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_images_users_username_fk` (`username`);

--
-- Indexes for table `user_lectures`
--
ALTER TABLE `user_lectures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_lectures_courses_id_fk` (`course_id`),
  ADD KEY `user_lectures_lectures_id_fk` (`lecture_id`),
  ADD KEY `user_lectures_users_username_fk` (`student`),
  ADD KEY `user_lectures_lectures_id_fk_2` (`prev_lecture_id`),
  ADD KEY `user_lectures_lectures_id_fk_3` (`next_lecture_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course_images`
--
ALTER TABLE `course_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course_ratings`
--
ALTER TABLE `course_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course_statuses`
--
ALTER TABLE `course_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `homeworks`
--
ALTER TABLE `homeworks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lectures`
--
ALTER TABLE `lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pending_approvals`
--
ALTER TABLE `pending_approvals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_courses`
--
ALTER TABLE `user_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_images`
--
ALTER TABLE `user_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_lectures`
--
ALTER TABLE `user_lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_lectures_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`id`);

--
-- Ограничения за таблица `authorities`
--
ALTER TABLE `authorities`
  ADD CONSTRAINT `authorities_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Ограничения за таблица `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_categories_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `courses_course_images_id_fk` FOREIGN KEY (`image_id`) REFERENCES `course_images` (`id`),
  ADD CONSTRAINT `courses_course_statuses_id_fk` FOREIGN KEY (`status_id`) REFERENCES `course_statuses` (`id`),
  ADD CONSTRAINT `courses_users_username_fk` FOREIGN KEY (`teacher`) REFERENCES `users` (`username`);

--
-- Ограничения за таблица `course_ratings`
--
ALTER TABLE `course_ratings`
  ADD CONSTRAINT `course_ratings_courses_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `course_ratings_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Ограничения за таблица `homeworks`
--
ALTER TABLE `homeworks`
  ADD CONSTRAINT `homeworks_courses_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `homeworks_lectures_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`id`),
  ADD CONSTRAINT `homeworks_users_username_fk` FOREIGN KEY (`student`) REFERENCES `users` (`username`);

--
-- Ограничения за таблица `lectures`
--
ALTER TABLE `lectures`
  ADD CONSTRAINT `lectures_assignmets_assignment_id_fk` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`),
  ADD CONSTRAINT `lectures_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `lectures_lectures_id_fk` FOREIGN KEY (`prev_lecture_id`) REFERENCES `lectures` (`id`);

--
-- Ограничения за таблица `pending_approvals`
--
ALTER TABLE `pending_approvals`
  ADD CONSTRAINT `pending_approvals_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Ограничения за таблица `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_user_images_id_fk` FOREIGN KEY (`image_id`) REFERENCES `user_images` (`id`);

--
-- Ограничения за таблица `user_courses`
--
ALTER TABLE `user_courses`
  ADD CONSTRAINT `user_courses_courses_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `user_courses_users_username_fk` FOREIGN KEY (`student`) REFERENCES `users` (`username`);

--
-- Ограничения за таблица `user_lectures`
--
ALTER TABLE `user_lectures`
  ADD CONSTRAINT `user_lectures_courses_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `user_lectures_lectures_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`id`),
  ADD CONSTRAINT `user_lectures_lectures_id_fk_2` FOREIGN KEY (`prev_lecture_id`) REFERENCES `lectures` (`id`),
  ADD CONSTRAINT `user_lectures_lectures_id_fk_3` FOREIGN KEY (`next_lecture_id`) REFERENCES `lectures` (`id`),
  ADD CONSTRAINT `user_lectures_users_username_fk` FOREIGN KEY (`student`) REFERENCES `users` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
