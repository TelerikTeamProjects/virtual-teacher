package com.company.virtualteacher.controllers;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.stream.Collectors;

public class SecurityDetailsProvider {
    protected String getLoggedUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    protected String getLoggedAuthority() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()).get(0);
    }
}
