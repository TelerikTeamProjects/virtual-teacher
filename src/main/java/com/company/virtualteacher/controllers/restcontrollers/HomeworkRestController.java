package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.FileValidator;
import com.company.virtualteacher.models.Homework;
import com.company.virtualteacher.models.HomeworkGradeDTO;
import com.company.virtualteacher.services.interfaces.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/enrolled/courses")
@Valid
public class HomeworkRestController extends FileValidator {
    private static final String FILE_COULD_NOT_BE_STORED = "File could not be stored. Please try again later!";

    private HomeworkService homeworkService;

    @Autowired
    public HomeworkRestController(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    @PostMapping("/{courseId}/lectures/{lectureId}/homework/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable int courseId,
                                         @PathVariable int lectureId, @RequestParam String username) throws IOException {
        String fileName = validateFileAndName(file, "(.doc|.docx|.pdf|.txt)");

        Homework homework;
        try {
            homework = new Homework(fileName, username, courseId, lectureId, file.getContentType(), file.getBytes());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, FILE_COULD_NOT_BE_STORED);
        }

        try {
            homeworkService.store(homework, getLoggedUsername());
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return fileName;
    }

    @GetMapping("/{courseId}/grades/average")
    public double getAverageGrade(@PathVariable int courseId,  @RequestParam String username) {
        try {
            return homeworkService.getUserAverageGradeByCourse(courseId, username, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{courseId}/lectures/{lectureId}/grade")
    public double getGrade(@PathVariable int lectureId,  @RequestParam String username) {
        try {
            return homeworkService.getUserGrade(lectureId, username, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{courseId}/assignments/count")
    public int getCountByCourse(@PathVariable int courseId) {
        try {
            return homeworkService.countByCourseId(courseId,getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{courseId}/lectures/{lectureId}/homework/{homeworkId}/download")
    public ResponseEntity<Resource> downloadFile(@PathVariable int homeworkId, @PathVariable int lectureId, @PathVariable int courseId) {
        try {
            Homework homework = homeworkService.getById(homeworkId, lectureId, courseId, getLoggedUsername());
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(homework.getFileType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + homework.getFileName() + "\"")
                    .body(new ByteArrayResource(homework.getFile()));
        } catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{courseId}/lectures/{lectureId}/homework/list")
    public List<Homework> getAllByLecture(@PathVariable int courseId, @PathVariable int lectureId) {
        try {
            return homeworkService.getByLectureId(lectureId, courseId, getLoggedUsername());
        } catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{courseId}/lectures/{lectureId}/grade")
    public HomeworkGradeDTO gradeHomework(@Valid @RequestBody HomeworkGradeDTO homeworkGradeDTO, @PathVariable int courseId, @PathVariable int lectureId) {
        try {
            homeworkService.gradeHomework(homeworkGradeDTO, lectureId, courseId, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return homeworkGradeDTO;
    }
}
