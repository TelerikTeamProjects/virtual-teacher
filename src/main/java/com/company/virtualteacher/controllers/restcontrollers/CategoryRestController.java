package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.FileValidator;
import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.services.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/categories")
@Valid
public class CategoryRestController extends FileValidator {
    private static final String FILE_COULD_NOT_BE_STORED = "File could not be stored. Please try again later!";

    private CategoryService categoryService;

    @Autowired
    public CategoryRestController(CategoryService service) {
        this.categoryService = service;
    }

    @PostMapping("/new")
    public Category create(@Valid @RequestBody Category category) {
        try {
            categoryService.create(category, getLoggedAuthority());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return category;
    }

    @PutMapping("/{id}")
    public Category update(@PathVariable int id,@Valid @RequestBody Category category) {
        try {
            category.setId(id);
            categoryService.update(category);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return category;
    }

    @PostMapping("/{categoryId}/images/upload/")
    public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable int categoryId) {
        validateFileAndName(file, "(.jpg|.jpeg|.png|.gif|.bmp)");
        Category category;

        try {
            category = categoryService.getById(categoryId);
            category.setImage(file.getBytes());
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, FILE_COULD_NOT_BE_STORED);
        }

        try {
            categoryService.update(category);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return file.getOriginalFilename();
    }
}
