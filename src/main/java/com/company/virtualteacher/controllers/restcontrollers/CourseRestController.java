package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseDTO;
import com.company.virtualteacher.repositories.CategoryCoursesCount;
import com.company.virtualteacher.services.CourseStatus;
import com.company.virtualteacher.services.interfaces.CoursePresentationService;
import com.company.virtualteacher.services.interfaces.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/courses")
@Valid
public class CourseRestController extends SecurityDetailsProvider {
    private CourseService courseService;
    private CoursePresentationService coursePresentationService;

    @Autowired
    public CourseRestController(CourseService courseService, CoursePresentationService coursePresentationService) {
        this.courseService = courseService;
        this.coursePresentationService = coursePresentationService;
    }

    @GetMapping
    public Page<CourseDTO> get(@RequestParam(required = false) String title,
                               @RequestParam(required = false) String teacher,
                               @RequestParam(required = false) String category,
                               @PageableDefault(size = 8) Pageable pageable) {
        return coursePresentationService.getCourses(title, teacher, category, pageable);
    }

    @GetMapping("/groupbycategory")
    public Page<CategoryCoursesCount> countAllDistinctByCategory(@PageableDefault(size = 6) Pageable pageable) {
        return coursePresentationService.countAllDistinctByCategory(pageable);
    }

    @GetMapping("/count")
    public int getTeachersCount() {
        return courseService.getCourses();
    }

    @PostMapping("/new")
    public Course create(@Valid @RequestBody Course course) {
        try {
            courseService.create(course, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return course;
    }

    @PutMapping("/{id}/draft/update")
    public Course updateDraft(@Valid @RequestBody Course course, @PathVariable int id) {
        try {
            courseService.updateDraft(course, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return course;
    }

    @PutMapping("/{id}/update")
    public Course update(@Valid @RequestBody Course course, @PathVariable int id) {
        try {
            courseService.update(course, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return course;
    }

    @PutMapping("/{id}/activate")
    public void activate(@PathVariable int id) {
        try {
            courseService.activateCourse(id, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/delete")
    public void delete(@PathVariable int id) {
        try {
            courseService.deleteCourse(id, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{teacher}")
    public List<Course> getByTeacher(@PathVariable String teacher) {
        if (!teacher.equals(getLoggedUsername())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Course not found.");
        }
        return coursePresentationService.getAllByTeacher(teacher);
    }

    @GetMapping("/{id}/avgrating")
    public double getAvgRating(@PathVariable int id) {
        try {
            Course course = courseService.getByIdAndStatus(id,  CourseStatus.STATUS_ACTIVE);
            return course.getAvgRating();
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }
}
