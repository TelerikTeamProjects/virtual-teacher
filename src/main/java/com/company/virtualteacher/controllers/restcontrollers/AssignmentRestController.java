package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.FileValidator;
import com.company.virtualteacher.models.Assignment;
import com.company.virtualteacher.services.interfaces.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/lectures")
@Valid
public class AssignmentRestController extends FileValidator {
    private static final String FILE_COULD_NOT_BE_STORED = "File could not be stored. Please try again later!";

    private AssignmentService assignmentService;

    @Autowired
    public AssignmentRestController(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @PostMapping("/{lectureId}/assignments/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable int lectureId) {
        validateFileAndName(file, "(.doc|.docx|.pdf|.txt)");
        Assignment assignment;

        try {
            assignment = new Assignment(lectureId, file.getOriginalFilename(), file.getContentType(), file.getBytes());
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, FILE_COULD_NOT_BE_STORED);
        }

        try {
            assignmentService.store(assignment, getLoggedUsername());
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return file.getOriginalFilename();
    }

    @GetMapping("/{lectureId}/assignments/{assignmentId}")
    public ResponseEntity<Resource> get(@PathVariable int assignmentId, @PathVariable int lectureId) {
        try {
            Assignment assignment = assignmentService.get(assignmentId, lectureId, getLoggedUsername());
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(assignment.getFileType()))
                    .body(new ByteArrayResource(assignment.getFile()));
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{lectureId}/assignments/{assignmentId}/name")
    public String getFileName(@PathVariable int assignmentId, @PathVariable int lectureId) {
        try {
            return assignmentService.getFileName(assignmentId, lectureId, getLoggedUsername(), getLoggedAuthority());
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{lectureId}/assignments/{assignmentId}/download")
    public ResponseEntity<Resource> downloadFile(@PathVariable int assignmentId, @PathVariable int lectureId) {
        try {
            Assignment assignment = assignmentService.get(assignmentId, lectureId, getLoggedUsername());
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(assignment.getFileType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + assignment.getFileName() + "\"")
                    .body(new ByteArrayResource(assignment.getFile()));
        }catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
