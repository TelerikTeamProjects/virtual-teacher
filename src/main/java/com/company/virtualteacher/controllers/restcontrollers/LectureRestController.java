package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.Lecture;
import com.company.virtualteacher.services.interfaces.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Valid
public class LectureRestController extends SecurityDetailsProvider {
    private LectureService lectureService;

    @Autowired
    public LectureRestController(LectureService lectureService) {
        this.lectureService = lectureService;
    }

    @PostMapping("/lectures/new")
    public Lecture create(@Valid @RequestBody Lecture lecture) {
        try {
            lectureService.create(lecture, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return lecture;
    }

    @PutMapping("/lectures/{id}/update")
    public Lecture update(@Valid @RequestBody Lecture lecture, @PathVariable int id){
        try {
            lectureService.update(lecture, id, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return lecture;
    }

    @GetMapping("/courses/{courseId}/lectures")
    public List<Lecture> getLectureListByCourseId(@PathVariable int courseId) {
        return lectureService.getLectureListByCourseId(courseId);
    }
}
