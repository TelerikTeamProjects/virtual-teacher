package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.UserLecture;
import com.company.virtualteacher.models.UserLectureProgressDTO;
import com.company.virtualteacher.services.interfaces.UserLecturesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/enrolled")
@Valid
public class UserLecturesRestController extends SecurityDetailsProvider {
    private UserLecturesService userLecturesService;

    @Autowired
    public UserLecturesRestController(UserLecturesService userLecturesService) {
        this.userLecturesService = userLecturesService;
    }

    @GetMapping("/courses/{courseId}/lectures")
    public List<UserLecture> getUserLectures(@PathVariable int courseId, @RequestParam String username) {
        try {
            return userLecturesService.getUserLectures(courseId, username, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/courses/{courseId}/lectures/{lectureId}/progress")
    public UserLectureProgressDTO getLectureProgress(@PathVariable int lectureId, @RequestParam String username) {
        try {
            UserLecture userLecture = userLecturesService.getByLectureIdAndStudent(lectureId, username, getLoggedUsername());
            return new UserLectureProgressDTO(userLecture.isVideoWatched(), userLecture.isHomeworkSubmitted());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/courses/{courseId}/lectures/{lectureId}/watched/set")
    public UserLecture setVideoWatched(@Valid @RequestBody UserLecture userLecture) {
        try {
            userLecturesService.setVideoWatched(userLecture.getLectureId(), userLecture.getStudent(), getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return userLecture;
    }

    @PutMapping("/courses/{courseId}/lectures/{lectureId}/available/set")
    public UserLecture setAvailable(@Valid @RequestBody UserLecture userLecture) {
        try {
            userLecturesService.setLectureAvailable(userLecture.getLectureId(), userLecture.getStudent(), getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return userLecture;
    }

    @GetMapping("/courses/{courseId}/progress/total")
    public int getProgress(@PathVariable int courseId, @RequestParam String username) {
        try {
            return userLecturesService.getProgressByCourseId(courseId, username, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
