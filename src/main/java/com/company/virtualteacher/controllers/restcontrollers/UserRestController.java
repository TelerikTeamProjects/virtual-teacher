package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.User;
import com.company.virtualteacher.models.UserCourse;
import com.company.virtualteacher.services.interfaces.UserPresentationService;
import com.company.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@Valid
public class UserRestController extends SecurityDetailsProvider {
    private UserService userService;
    private UserPresentationService userPresentationService;

    @Autowired
    public UserRestController(UserService userService, UserPresentationService userPresentationService) {
        this.userService = userService;
        this.userPresentationService = userPresentationService;
    }

    @GetMapping("/students/count")
    public int getStudentsCount() {
        return userPresentationService.getStudents().size();
    }

    @GetMapping("/teachers/count")
    public int getTeachersCount() {
        return userPresentationService.getTeachers().size();
    }

    @GetMapping("/{username}")
    public User getUserByUserName(@PathVariable String username){
        return userService.getDetails(username);
    }

    @GetMapping
    public List<User> get() {
        return userPresentationService.findAll();
    }

    @GetMapping("{username}/ongoingcourses")
    public List<UserCourse> getOngoingCourses(@PathVariable String username) {
        return userPresentationService.getOngoingCourses(username);
    }

    @GetMapping("{username}/completedcourses")
    public List<UserCourse> getCompletedCourses(@PathVariable String username) {
        return userPresentationService.getCompletedCourses(username);
    }

    @PutMapping("/{username}/details")
    public User update(@PathVariable String username, @Valid @RequestBody User user) {
        if (!user.getUsername().equals(username)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized request.");
        }

        try {
            userService.update(user);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return user;
    }
}

