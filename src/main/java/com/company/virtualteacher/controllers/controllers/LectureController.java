package com.company.virtualteacher.controllers.controllers;

import com.company.virtualteacher.models.Lecture;
import com.company.virtualteacher.services.interfaces.LectureService;
import com.company.virtualteacher.services.interfaces.UserLecturesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class LectureController {
    private static final String LECTURE_NOT_FOUND = "Lecture not found.";
    private static final String PAGE_NOT_AVAILABLE = "This page is not available.";

    private LectureService lectureService;
    private UserLecturesService userLecturesService;

    @Autowired
    public LectureController(LectureService lectureService, UserLecturesService userLecturesService) {
        this.lectureService = lectureService;
        this.userLecturesService = userLecturesService;
    }

    @GetMapping("courses/{courseId}/lectures/{lectureId}")
    public String getLecture(@PathVariable int courseId, @PathVariable int lectureId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!userLecturesService.isLectureAvailable(lectureId, authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, PAGE_NOT_AVAILABLE);
        }
        Lecture lectureToShow = getLectureById(lectureId);
        if (lectureToShow.getCourseId() != courseId) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, LECTURE_NOT_FOUND);
        }
        model.addAttribute("lecture", lectureToShow);

        return "lecture";
    }

    private Lecture getLectureById(int id) {
        try {
            return lectureService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
