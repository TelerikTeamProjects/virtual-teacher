package com.company.virtualteacher.controllers.controllers;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class LoginController {
    @GetMapping("/login")
    public String showLogin() {
        List principal = (List) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if(((SimpleGrantedAuthority) principal.get(0)).getAuthority().contains("ROLE_ANONYMOUS")){
            return "login";
        }else if(((SimpleGrantedAuthority) principal.get(0)).getAuthority().contains("ROLE_ADMIN")){
            return "redirect:/admin";
        }else{
            return "redirect:/";
        }

    }

    @GetMapping("/access-denied")
    public String accessDenied(){
        return "access-denied";
    }
}

