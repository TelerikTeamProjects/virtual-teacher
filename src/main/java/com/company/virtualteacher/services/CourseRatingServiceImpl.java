package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseRating;
import com.company.virtualteacher.models.UserCourse;
import com.company.virtualteacher.repositories.CourseRatingRepository;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.UserCourseRepository;
import com.company.virtualteacher.services.interfaces.CourseRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseRatingServiceImpl implements CourseRatingService {
    private static final String USERNAME_DOES_NOT_MATCH = "Username does not match.";

    private CourseRatingRepository courseRatingRepository;
    private CourseRepository courseRepository;
    private UserCourseRepository userCourseRepository;

    @Autowired
    public CourseRatingServiceImpl(CourseRatingRepository courseRatingRepository, CourseRepository courseRepository, UserCourseRepository userCourseRepository) {
        this.courseRatingRepository = courseRatingRepository;
        this.courseRepository = courseRepository;
        this.userCourseRepository = userCourseRepository;
    }

    @Override
    public void add(CourseRating courseRating, String username) {
        checkUsername(courseRating.getUsername(), username);

        Course course = courseRepository.findByIdAndStatus(courseRating.getCourseId(),  CourseStatus.STATUS_ACTIVE);
        if (course == null) {
            throw new IllegalArgumentException(String.format("Course with id %d was nor found.", courseRating.getCourseId()));
        }
        UserCourse userCourse = userCourseRepository.findByCourse_IdAndStudent(courseRating.getCourseId(), courseRating.getUsername());
        if (userCourse == null || !userCourse.isCompleted()) {
            throw new IllegalArgumentException(String.format("Student '%s' have not completed course with id %d.", courseRating.getUsername(), courseRating.getCourseId()));
        }
        if (courseRatingRepository.findByCourseIdAndUsername(courseRating.getCourseId(), courseRating.getUsername()) != null) {
            throw new IllegalArgumentException(String.format("Student '%s' have already rated course with id %d.", courseRating.getUsername(), courseRating.getCourseId()));
        }

        courseRatingRepository.save(courseRating);
        double avgRating = courseRatingRepository.getAverageRating(courseRating.getCourseId());
        course.setAvgRating(avgRating);
        courseRepository.save(course);
    }

    @Override
    public int getUserRating(int courseId, String username, String loggedUser) {
        checkUsername(username, loggedUser);

        CourseRating courseRating = courseRatingRepository.findByCourseIdAndUsername(courseId, username);
        if (courseRating == null) {
            return 0;
        }
        return courseRating.getRating();
    }

    private void checkUsername(String username, String loggedUser) {
        if (!loggedUser.equals(username)) {
            throw new IllegalArgumentException(USERNAME_DOES_NOT_MATCH);
        }
    }
}
