package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.Lecture;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.services.interfaces.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    private static final String COURSE_NOT_FOUND = "Course not found.";
    private static final String COURSE_ALREADY_EXISTS = "Course with title '%s' already exists.";
    private static final String USERNAME_DOES_NOT_MATCH = "Username does not match.";
    private static final String PLEASE_PROVIDE_AN_IMAGE = "Please provide an image for the course.";
    private static final String AT_LEAST_ONE_LECTURE = "The course must have at least one lecture.";
    private static final String ALL_LECTURES_MUST_HAVE_AN_ASSIGNMENT = "All lectures must have an assignment.";

    private CourseRepository courseRepository;
    private LectureRepository lectureRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, LectureRepository lectureRepository) {
        this.courseRepository = courseRepository;
        this.lectureRepository = lectureRepository;
    }

    @Override
    public Course getByTitle(String title) {
        return courseRepository.findCourseByTitle(title);
    }

    @Override
    public Course getByIdAndStatus(int id, int statusId) {
        Course course = courseRepository.findByIdAndStatus(id, statusId);
        if (course == null) {
            throw new IllegalArgumentException(COURSE_NOT_FOUND);
        }
        return course;
    }

    @Override
    public void create(Course course, String username) {
        checkUsername(course, username);

        checkCourseTitle(course.getTitle(), course.getId());

        course.setStatus(CourseStatus.STATUS_DRAFT);
        courseRepository.save(course);
    }

    @Override
    public void updateDraft(Course course, String username) {
        Course existingCourse = getCourse(course.getId(), CourseStatus.STATUS_DRAFT);
        checkUsername(existingCourse, username);
        checkCourseTitle(course.getTitle(), course.getId());

        course.setImage(existingCourse.getImage());
        course.setStatus(CourseStatus.STATUS_DRAFT);
        courseRepository.save(course);
    }

    @Override
    public void update(Course course, String username) {
        Course existingCourse = getCourse(course.getId(), CourseStatus.STATUS_ACTIVE);
        checkUsername(existingCourse, username);

        existingCourse.setDescription(course.getDescription());
        existingCourse.setProgram(course.getProgram());
        courseRepository.save(existingCourse);
    }

    @Override
    public void activateCourse(int courseId, String username) {
        Course currentCourse = getCourse(courseId, CourseStatus.STATUS_DRAFT);
        checkUsername(currentCourse, username);

        if (currentCourse.getImage() == null) {
            throw new IllegalArgumentException(PLEASE_PROVIDE_AN_IMAGE);
        }

        List<Lecture> lectures = lectureRepository.findAllByCourseIdAndIsActiveTrue(courseId);
        if (lectures.isEmpty()) {
            throw new IllegalArgumentException(AT_LEAST_ONE_LECTURE);
        }
        if (!lectures.stream().allMatch(l -> l.getAssignmentId() != null)) {
            throw new IllegalArgumentException(ALL_LECTURES_MUST_HAVE_AN_ASSIGNMENT);
        }

        courseRepository.activateCourse(courseId);
    }

    @Override
    public void deleteCourse(int courseId, String username) {
        Course course = courseRepository.findById(courseId).orElseThrow(() -> new IllegalArgumentException(COURSE_NOT_FOUND));
        checkUsername(course, username);
        if (course.getStatus() != CourseStatus.STATUS_DRAFT && course.getStatus() != CourseStatus.STATUS_ACTIVE) {
            throw new IllegalArgumentException(COURSE_NOT_FOUND);
        }

        courseRepository.deleteCourse(course.getId());
    }

    @Override
    public void deleteCourse(int courseId) {
        courseRepository.deleteCourse(courseId);
    }

    @Override
    public int getCourses() {
        return courseRepository.countCoursesByStatus(CourseStatus.STATUS_ACTIVE);
    }

    private void checkCourseTitle(String title, int courseId) {
        Course course = courseRepository.findCourseByTitle(title);
        if (course != null && course.getId() != courseId) {
            throw new IllegalArgumentException(String.format(COURSE_ALREADY_EXISTS, title));
        }
    }

    private void checkUsername(Course course, String username) {
        if (!course.getTeacher().equals(username)) {
            throw new IllegalArgumentException(USERNAME_DOES_NOT_MATCH);
        }
    }

    private Course getCourse(int courseId, int status) {
        Course course = courseRepository.findByIdAndStatus(courseId, status);
        if (course == null) {
            throw new IllegalArgumentException(COURSE_NOT_FOUND);
        }
        return course;
    }
}
