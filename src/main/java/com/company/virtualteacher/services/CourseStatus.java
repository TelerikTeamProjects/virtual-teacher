package com.company.virtualteacher.services;

public class CourseStatus {
    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_DRAFT = 2;
    public static final int STATUS_DELETED = 3;
}