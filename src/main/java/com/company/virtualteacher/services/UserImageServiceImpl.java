package com.company.virtualteacher.services;

import com.company.virtualteacher.models.UserImage;
import com.company.virtualteacher.repositories.UserImageRepository;
import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.interfaces.UserImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserImageServiceImpl implements UserImageService {
    private UserImageRepository userImageRepository;
    private UserRepository userRepository;

    @Autowired
    public UserImageServiceImpl(UserImageRepository userImageRepository, UserRepository userRepository) {
        this.userImageRepository = userImageRepository;
        this.userRepository = userRepository;
    }

    @Override
    public UserImage storeFile(UserImage image, String username) {
        if (userRepository.findUserByUsername(username) == null) {
            throw new IllegalArgumentException(String.format("User with id %s not found.", username));
        }

        userImageRepository.save(image);
        userRepository.updateImageId(username, image.getId());
        return image;
    }

    @Override
    public UserImage getFile(int fileId) {
        return userImageRepository.findById(fileId)
                .orElseThrow(() -> new IllegalArgumentException("File not found with image id " + fileId));
    }
}
