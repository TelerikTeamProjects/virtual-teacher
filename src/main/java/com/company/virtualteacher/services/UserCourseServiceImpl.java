package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.UserCourse;
import com.company.virtualteacher.models.UserLecture;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.repositories.UserCourseRepository;
import com.company.virtualteacher.repositories.UserLectureRepository;
import com.company.virtualteacher.services.interfaces.UserCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserCourseServiceImpl implements UserCourseService {
    private static final String THIS_COURSE_IS_ENROLLED = "This course is enrolled by %s";
    private static final String USERNAME_DOES_NOT_MATCH = "Username does not match.";

    private UserCourseRepository userCourseRepository;
    private LectureRepository lectureRepository;
    private UserLectureRepository userLectureRepository;
    private CourseRepository courseRepository;

    @Autowired
    public UserCourseServiceImpl(UserCourseRepository userCourseRepository, LectureRepository lectureRepository, UserLectureRepository userLectureRepository, CourseRepository courseRepository) {
        this.userCourseRepository = userCourseRepository;
        this.lectureRepository = lectureRepository;
        this.userLectureRepository = userLectureRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public boolean isStudentEnrolled(int courseId, String username) {
        return userCourseRepository.findByCourse_IdAndStudent(courseId, username) != null;
    }

    @Override
    public void enroll(UserCourse userCourse) {
        if (userCourseRepository.findByCourse_IdAndStudent(userCourse.getCourse().getId(), userCourse.getStudent()) != null) {
            throw new IllegalArgumentException(String.format(THIS_COURSE_IS_ENROLLED, userCourse.getStudent()));
        }

        List<UserLecture> userLectures = lectureRepository.findAllByCourseIdAndIsActiveTrue(userCourse.getCourse().getId()).stream()
                .map(l -> new UserLecture(l.getId(),l.getPrevLectureId(), l.getNextLectureId(), l.getCourseId(),
                        userCourse.getStudent(), false, false, false))
                .collect(Collectors.toList());
        userLectures.get(0).setAvailable(true);
        userLectureRepository.saveAll(userLectures);

        userCourseRepository.save(userCourse);
    }

    @Override
    public int getEnrolledCount(int courseId, String username) {
        Course course = courseRepository.findByIdAndStatus(courseId, CourseStatus.STATUS_ACTIVE);
        if (course == null) {
            throw new IllegalArgumentException("Course not found.");
        }
        if (!username.equals(course.getTeacher())) {
            throw new IllegalArgumentException(String.format("Course with id %d does not belong to %s.", courseId, username));
        }
        Integer count = userCourseRepository.countByCourseAndCompletedIsFalse(courseId);
        return count != null ? count : 0;
    }

    @Override
    public int countUserCoursesByCourseId(int courseId) {
        return userCourseRepository.countUserCoursesByCourseId(courseId);
    }

    @Override
    public boolean isCompleted(int courseId, String username, String loggedUser) {
        if (!loggedUser.equals(username)) {
            throw new IllegalArgumentException(USERNAME_DOES_NOT_MATCH);
        }
        UserCourse userCourse = userCourseRepository.findByCourse_IdAndStudent(courseId, username);
        return userCourse == null ? false : userCourse.isCompleted();
    }
}
