package com.company.virtualteacher.services;

import com.company.virtualteacher.models.User;
import com.company.virtualteacher.models.UserCourse;
import com.company.virtualteacher.models.UserManagementDTO;
import com.company.virtualteacher.repositories.UserCourseRepository;
import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.interfaces.UserPresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserPresentationServiceImpl implements UserPresentationService {
    private UserRepository userRepository;
    private UserCourseRepository userCourseRepository;

    @Autowired
    public UserPresentationServiceImpl(UserRepository userRepository, UserCourseRepository userCourseRepository) {
        this.userRepository = userRepository;
        this.userCourseRepository = userCourseRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAllByEnabledIsTrue();
    }

    @Override
    public List<UserCourse> getOngoingCourses(String studentName) {
        return userCourseRepository.findAllByStudentAndIsCompletedIsFalse(studentName);
    }

    @Override
    public List<UserCourse> getCompletedCourses(String studentName) {
        return userCourseRepository.findAllByStudentAndIsCompletedIsTrue(studentName);
    }

    @Override
    public List<String> getTeachers() {
        return userRepository.getTeachers();
    }

    @Override
    public List<String> getStudents() {
        return userRepository.getStudents();
    }

    @Override
    public Page<UserManagementDTO> getStudentsList(Pageable pageable) {
        Page<User> entities = userRepository.getStudentsList(pageable);
        Page<UserManagementDTO> studentsDTOPage = entities.map(this::convertToUserManagementDTO);
        return studentsDTOPage;
    }

    @Override
    public Page<UserManagementDTO> getUsersListByUsernameAndRole(String username, String role, Pageable pageable) {
        if(username == null || username.trim().equals("")){
            username = "";
        }

        if(role == null || role.trim().equals("")){
            role = "";
        }

        Page<User> entities = userRepository.getUsersListByUsernameAndRole(username, role, pageable);
        Page<UserManagementDTO> studentsDTOPage = entities.map(this::convertToUserManagementDTO);
        return studentsDTOPage;
    }

    private UserManagementDTO convertToUserManagementDTO(User user) {
        UserManagementDTO userManagementDTO = new UserManagementDTO();
        userManagementDTO.setUsername(user.getUsername());
        userManagementDTO.setImage(user.getImage());

        return userManagementDTO;
    }
}
