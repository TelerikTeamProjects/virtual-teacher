package com.company.virtualteacher.services;

import com.company.virtualteacher.models.*;
import com.company.virtualteacher.repositories.*;
import com.company.virtualteacher.services.interfaces.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeworkServiceImpl implements HomeworkService {
    private static final String USERNAME_DOES_NOT_MATCH = "Username does not match.";
    private static final String COURSE_NOT_FOUND = "Course not found.";

    private HomeworkRepository homeworkRepository;
    private UserLectureRepository userLectureRepository;
    private UserCourseRepository userCourseRepository;
    private CourseRepository courseRepository;
    private LectureRepository lectureRepository;

    @Autowired
    public HomeworkServiceImpl(HomeworkRepository homeworkRepository, UserLectureRepository userLectureRepository,
                               UserCourseRepository userCourseRepository, CourseRepository courseRepository, LectureRepository lectureRepository) {
        this.homeworkRepository = homeworkRepository;
        this.userLectureRepository = userLectureRepository;
        this.userCourseRepository = userCourseRepository;
        this.courseRepository = courseRepository;
        this.lectureRepository = lectureRepository;
    }

    @Override
    public Homework store(Homework homework, String username) {
        checkUsername(homework.getStudent(), username);
        UserLecture userLecture = userLectureRepository.findByLectureIdAndStudentAndAvailable(homework.getLectureId(),
                homework.getStudent(), true);
        if (userLecture == null) {
            throw new IllegalArgumentException(String.format("Student %s does not have assignment for lecture with id %d.",
                    homework.getStudent(), homework.getLectureId()));
        }

        if (homeworkRepository.findByLectureIdAndStudent(homework.getLectureId(), homework.getStudent()) != null) {
            throw new IllegalArgumentException(String.format("Student %s has already submitted the homework for lecture with id %d.",
                    homework.getStudent(), homework.getLectureId()));
        }

        homeworkRepository.save(homework);
        userLecture.setHomeworkSubmitted(true);
        userLectureRepository.save(userLecture);
        if (userLecture.getNextLectureId() != null) {
            UserLecture nextUserLecture =
                    userLectureRepository.findByLectureIdAndStudentAndAvailable(userLecture.getNextLectureId(),
                    homework.getStudent(), false);
            nextUserLecture.setAvailable(true);
            userLectureRepository.save(nextUserLecture);
        }
        return homework;
    }

    @Override
    public double getUserAverageGradeByCourse(int courseId, String username, String loggedUser) {
        checkUsername(username, loggedUser);
        Double grade  = homeworkRepository.getUsersAverageGradeByCourse(courseId, username);
        return grade != null ? grade : 0.0;
    }

    @Override
    public double getUserGrade(int lectureId, String username, String loggedUser) {
        checkUsername(username, loggedUser);
        Homework homework = homeworkRepository.findByLectureIdAndStudent(lectureId, username);
        return (homework != null && homework.getGrade() != null) ? homework.getGrade() : 0.0;
    }

    @Override
    public int countByCourseId(int courseId, String loggedUser) {
        Course course = getCourse(courseId, CourseStatus.STATUS_ACTIVE);
        checkUsername(course.getTeacher(), loggedUser);

        return homeworkRepository.countByCourseIdAndGradeIsNull(courseId);
    }

    @Override
    public Homework getById(int homeworkId, int lectureId, int courseId, String loggedUser) {
        Course course = getCourse(courseId, CourseStatus.STATUS_ACTIVE);
        checkUsername(course.getTeacher(), loggedUser);

        Homework homework = homeworkRepository.findById(homeworkId)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Homework with id %d was not found.", homeworkId)));
        if (homework.getLectureId() != lectureId) {
            throw new IllegalArgumentException(String.format("Homework with id %d was not found.", homeworkId));
        }
        return homework;
    }

    @Override
    public List<Homework> getByLectureId(int lectureId, int courseId, String loggedUser) {
        Course course = getCourse(courseId, CourseStatus.STATUS_ACTIVE);
        checkUsername(course.getTeacher(), loggedUser);
        checkLecture(lectureId, courseId);

        return homeworkRepository.findAllByLectureIdAndGradeIsNull(lectureId);
    }

    @Override
    public void gradeHomework(HomeworkGradeDTO homeworkGradeDTO, int lectureId, int courseId, String loggedUser) {
        Course course = getCourse(courseId, CourseStatus.STATUS_ACTIVE);
        checkUsername(course.getTeacher(), loggedUser);
        checkLecture(lectureId, courseId);

        Homework current = homeworkRepository.findById(homeworkGradeDTO.getId())
                .orElseThrow(() -> new IllegalArgumentException(String.format("Homework with id %d was not found.", homeworkGradeDTO.getId())));
        homeworkRepository.gradeHomework(current.getId(),homeworkGradeDTO.getGrade());
        if (homeworkRepository.countByCourseIdAndStudentAndGradeIsNull(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent()) == 0) {
            if (homeworkRepository.getUsersAverageGradeByCourse(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent()) >= 3.0) {
                userCourseRepository.setCompleted(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent());
            }
        }
    }

    private void checkUsername(String username, String loggedUser) {
        if (!loggedUser.equals(username)) {
            throw new IllegalArgumentException(USERNAME_DOES_NOT_MATCH);
        }
    }

    private Course getCourse(int courseId, int status) {
        Course course = courseRepository.findByIdAndStatus(courseId, status);
        if (course == null) {
            throw new IllegalArgumentException(COURSE_NOT_FOUND);
        }
        return course;
    }

    private void checkLecture(int lectureId, int courseId) {
        Lecture lecture = lectureRepository.findById(lectureId).orElseThrow(() -> new IllegalArgumentException(String.format("Lecture with id %d was not found.", lectureId)));
        if (lecture.getCourseId() != courseId) {
            throw new IllegalArgumentException(String.format("Lecture with id %d does not belong to course with id %d", lectureId, courseId));
        }
    }
}
