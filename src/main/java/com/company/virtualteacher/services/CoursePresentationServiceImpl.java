package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseDTO;
import com.company.virtualteacher.repositories.CategoryCoursesCount;
import com.company.virtualteacher.repositories.CategoryRepository;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.services.interfaces.CoursePresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CoursePresentationServiceImpl implements CoursePresentationService {
    private CourseRepository courseRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public CoursePresentationServiceImpl(CourseRepository courseRepository, CategoryRepository categoryRepository) {
        this.courseRepository = courseRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Page<CourseDTO> getCourses(String title, String teacher, String category, Pageable pageable) {
        Page<CourseDTO> courseDTOPage = null;
        Page<Course> entities = null;

        if (title == null) {
            title = "";
        }

        if (teacher == null) {
            teacher = "";
        }

        if (category != null && !category.equals("")) {
            Category courseCategory = categoryRepository.findCategoryByName(category);
            entities = courseRepository.findAllByTitleContainingAndTeacherContainingAndCategoryAndStatus
                    (title, teacher, courseCategory, CourseStatus.STATUS_ACTIVE, pageable);
            courseDTOPage = entities.map(this::convertToCourseDTO);

            return courseDTOPage;

        }

        entities = courseRepository.findAllByTitleContainingAndTeacherContainingAndStatus
                (title, teacher, CourseStatus.STATUS_ACTIVE, pageable);
        courseDTOPage = entities.map(this::convertToCourseDTO);

        return courseDTOPage;
    }

    @Override
    public Page<CourseDTO> getCoursesByTitle(String title, Pageable pageable) {
        if (title == null) {
            title = "";
        }

        String teacher = "";
        Page<CourseDTO> courseDTOPage = null;
        Page<Course> entities = null;

        entities = courseRepository.findAllByTitleContainingAndTeacherContainingAndStatus
                (title, teacher, CourseStatus.STATUS_ACTIVE, pageable);
        courseDTOPage = entities.map(this::convertToCourseDTO);

        return courseDTOPage;
    }

    @Override
    public List<CategoryCoursesCount> topCategories() {
        return courseRepository.getTopCategories().stream().limit(3).collect(Collectors.toList());
    }

    @Override
    public Page<CategoryCoursesCount> countAllDistinctByCategory(Pageable pageable) {
        return courseRepository.countAllDistinctByCategory(pageable);
    }

    @Override
    public List<Course> getAllByTeacher(String teacher) {
        return courseRepository.findByTeacherAndStatusNot(teacher, CourseStatus.STATUS_DELETED);
    }

    private CourseDTO convertToCourseDTO(Course course) {
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(course.getId());
        courseDTO.setTitle(course.getTitle());
        courseDTO.setCategory(course.getCategory());
        courseDTO.setDescription(course.getDescription());
        courseDTO.setTeacher(course.getTeacher());
        courseDTO.setAvgRating(course.getAvgRating());
        courseDTO.setImage(course.getImage());

        return courseDTO;
    }
}