package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.Course;

public interface CourseService {
    void create(Course course, String username);

    Course getByTitle(String title);

    Course getByIdAndStatus(int id, int statusId);

    void update(Course course, String username);

    void updateDraft(Course course, String username);

    void activateCourse(int courseId, String username);

    void deleteCourse(int courseId, String username);

    void deleteCourse(int courseId);

    int getCourses();
}
