package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.UserLecture;

import java.util.List;

public interface UserLecturesService {
    List<UserLecture> getUserLectures(int courseId, String student, String loggedUser);

    boolean isLectureAvailable(int lectureId, String student);

    void setLectureAvailable(int lectureId, String student, String loggedUser);

    UserLecture getByLectureIdAndStudent(int lectureId, String student, String loggedUser);

    void setVideoWatched(int lectureId, String student, String loggedUser);

    int getProgressByCourseId(int courseId, String student, String loggedUser);
}
