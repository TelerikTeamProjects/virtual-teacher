package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.PendingApprovals;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PendingApprovalService {
    void approve(String username);

    Page<PendingApprovals> getPendingApprovals(String username, Pageable pageable);
}
