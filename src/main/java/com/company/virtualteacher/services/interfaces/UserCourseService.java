package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.UserCourse;

public interface UserCourseService {
    boolean isStudentEnrolled(int courseId, String username);

    void enroll(UserCourse userCourse);

    int getEnrolledCount(int courseId, String username);

    int countUserCoursesByCourseId(int courseId);

    boolean isCompleted(int courseId, String username, String loggedUser);
}
