package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.CourseImage;

public interface CourseImageService {
    void store(CourseImage courseImage, String teacher);

    CourseImage getByCourseId(int courseId);
}
