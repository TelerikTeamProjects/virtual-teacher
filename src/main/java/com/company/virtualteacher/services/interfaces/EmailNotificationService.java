package com.company.virtualteacher.services.interfaces;

import javax.mail.MessagingException;

public interface EmailNotificationService {
    void sendEmail(String username) throws MessagingException;
}
