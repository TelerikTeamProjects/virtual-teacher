package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.UserImage;

public interface UserImageService {
    UserImage storeFile(UserImage image, String username);

    UserImage getFile(int fileId);
}
