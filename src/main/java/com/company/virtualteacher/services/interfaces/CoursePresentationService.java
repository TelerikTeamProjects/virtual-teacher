package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseDTO;
import com.company.virtualteacher.repositories.CategoryCoursesCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CoursePresentationService {
    Page<CourseDTO> getCoursesByTitle(String title, Pageable pageable);

    Page<CourseDTO> getCourses(String title, String teacher, String category, Pageable pageable);

    Page<CategoryCoursesCount> countAllDistinctByCategory(Pageable pageable);

    List<CategoryCoursesCount> topCategories();

    List<Course> getAllByTeacher(String teacher);
}

