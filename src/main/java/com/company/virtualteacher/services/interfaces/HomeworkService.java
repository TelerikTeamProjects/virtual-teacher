package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.Homework;
import com.company.virtualteacher.models.HomeworkGradeDTO;

import java.util.List;

public interface HomeworkService {
    Homework store(Homework homework, String username);

    double getUserAverageGradeByCourse(int courseId, String username, String loggedUser);

    double getUserGrade(int lectureId, String username, String loggedUser);

    int countByCourseId(int courseId, String loggedUser);

    Homework getById(int homeworkId, int lectureId, int courseId, String loggedUser);

    List<Homework> getByLectureId(int lectureId, int courseId, String loggedUser);

    void gradeHomework(HomeworkGradeDTO homeworkGradeDTO, int lectureId, int courseId, String loggedUser);
}
