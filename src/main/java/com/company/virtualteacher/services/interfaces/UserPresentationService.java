package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.User;
import com.company.virtualteacher.models.UserCourse;
import com.company.virtualteacher.models.UserManagementDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserPresentationService {
    List<User> findAll();

    List<UserCourse> getOngoingCourses(String studentName);

    List<UserCourse> getCompletedCourses(String studentName);

    List<String> getTeachers();

    List<String> getStudents();

    Page<UserManagementDTO> getStudentsList(Pageable pageable);

    Page<UserManagementDTO> getUsersListByUsernameAndRole(String username, String role, Pageable pageable);
}
