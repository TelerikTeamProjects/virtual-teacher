package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.Lecture;

import java.util.List;

public interface LectureService {
    Lecture getByTitle(String title);

    Lecture getById(int id);

    void create(Lecture lecture, String username);

    void update(Lecture lecture, int lectureId, String username);

    int countLecturesByCourseId(int courseId);

    List<Lecture> getLectureListByCourseId(int courseId);
}
