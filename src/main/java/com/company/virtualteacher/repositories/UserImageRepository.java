package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserImageRepository extends JpaRepository<UserImage, Integer> {
}