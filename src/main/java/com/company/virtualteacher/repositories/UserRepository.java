package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserByUsername(String username);

    List<User> findAllByEnabledIsTrue();

    @Modifying
    @Transactional
    @Query(value = "update users set image_id = :imageId where username = :username", nativeQuery = true)
    void updateImageId(@Param("username") String username, @Param("imageId") int imageId);

    @Transactional
    @Query(value = "select u.username from users u " +
            "join authorities a on u.username = a.username " +
            "where u.enabled = true and a.authority = 'ROLE_TEACHER'", nativeQuery = true)
    List<String> getTeachers();

    @Modifying
    @Transactional
    @Query(value = "update users set email = :email, first_name = :firstName, middle_name = :middleName," +
            " last_name = :lastName where  username = :username", nativeQuery = true)
    void updateDetails(@Param("username") String username, @Param("email") String email, @Param("firstName") String firstName,
                       @Param("middleName") String middleName, @Param("lastName") String lastName);

    @Transactional
    @Query(value = "select u.username from users u " +
            "join authorities a on u.username = a.username " +
            "where u.enabled = true and a.authority = 'ROLE_STUDENT'", nativeQuery = true)
    List<String> getStudents();

    @Transactional
    @Query(value = "select * from users u " +
            "join authorities a on u.username = a.username " +
            "where u.enabled = true and a.authority = 'ROLE_STUDENT' order by u.username asc", nativeQuery = true)
    Page<User> getStudentsList(Pageable pageable);

    @Transactional
    @Query(value = "select * from users u " +
            "join authorities a on u.username = a.username " +
            "where u.enabled = true and a.authority = :role " +
            "and u.username like %:username% order by u.username asc", nativeQuery = true)
    Page<User> getUsersListByUsernameAndRole(@Param("username") String username, @Param("role") String role, Pageable pageable);
}