package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.models.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Integer> {
    Course findCourseByTitle(String name);

    Page<Course> findAllByTitleContainingAndTeacherContainingAndStatus
            (String title, String teacher, int status, Pageable pageable);

    Page<Course> findAllByTitleContainingAndTeacherContainingAndCategoryAndStatus
            (String title, String teacher, Category category, int status, Pageable pageable);

    @Transactional
    @Query("select c.category as category, count(c) as count " +
            "from Course c " +
            "where c.status = 1 " +
            "group by c.category.id " +
            "order by 2 desc")
    Page<CategoryCoursesCount> countAllDistinctByCategory(Pageable pageable);

    @Transactional
    @Query("select c.category as category, count(c) as count " +
            "from Course c " +
            "where c.status = 1 " +
            "group by c.category.id " +
            "order by 2 desc")
    List<CategoryCoursesCount> getTopCategories();

    Course findByIdAndStatus(int id, int statusId);

    @Modifying
    @Transactional
    @Query(value = "update courses set image_id = :imageId where id = :courseId", nativeQuery = true)
    void updateImageId(@Param("courseId") int courseId, @Param("imageId") int imageId);

    @Modifying
    @Transactional
    @Query(value = "update courses set status_id = 1 where id = :courseId", nativeQuery = true)
    void activateCourse(@Param("courseId") int courseId);

    int countCoursesByStatus(int courseStatus);

    @Modifying
    @Transactional
    @Query(value = "update courses set status_id = 3 where id = :courseId", nativeQuery = true)
    void deleteCourse(@Param("courseId") int courseId);

    List<Course> findByTeacherAndStatusNot(String teacher, int statusId);
}
