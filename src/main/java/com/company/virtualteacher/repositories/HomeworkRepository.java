package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.Homework;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface HomeworkRepository extends JpaRepository<Homework, Integer> {
    Homework findByLectureIdAndStudent(int lectureId, String student);

    @Query(value = "select avg(grade) as avg " +
            "from homeworks h " +
            "where h.course_id = :courseId and h.student = :student and grade is not null " +
            "group by h.course_id", nativeQuery = true)
    Double getUsersAverageGradeByCourse(@Param("courseId") int courseId, @Param("student") String username);

    Integer countByCourseIdAndGradeIsNull(int courseId);

    Integer countByLectureIdAndGradeIsNull(int lectureId);

    Integer countByCourseIdAndStudentAndGradeIsNull(int courseId, String student);

    List<Homework> findAllByLectureIdAndGradeIsNull(int lectureId);

    @Modifying
    @Transactional
    @Query(value = "update homeworks set grade = :grade where id = :id", nativeQuery = true)
    void gradeHomework(@Param("id") int id, @Param("grade") double grade);
}
