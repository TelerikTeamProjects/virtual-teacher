package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.UserLecture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserLectureRepository extends JpaRepository<UserLecture, Integer> {
    List<UserLecture> findAllByCourseIdAndStudentOrderByLectureId(int courseId, String student);

    UserLecture findByLectureIdAndStudentAndAvailable(int lectureId, String student, boolean available);
}
