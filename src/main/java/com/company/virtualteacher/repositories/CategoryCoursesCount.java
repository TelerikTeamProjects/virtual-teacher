package com.company.virtualteacher.repositories;

import org.springframework.beans.factory.annotation.Value;

public interface CategoryCoursesCount {
    @Value("#{target.category.id}")
    int getCategoryId();

    @Value("#{target.category.name}")
    String getCategoryName();

    @Value("#{target.category.image}")
    byte[] getCategoryImage();

    @Value("#{target.count}")
    int getCount();

    void setCategoryId(int id);

    void setCategoryName(String name);

    void setCategoryImage(byte[] image);

    void setCount(int count);
}
