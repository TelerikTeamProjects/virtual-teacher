package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Category findCategoryByName(String name);

    List<Category> findAllByOrderByNameAsc();

}
