package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LectureRepository extends JpaRepository<Lecture, Integer> {
    Lecture findLectureByTitle(String title);

    int countAllByCourseId(int id);

    List<Lecture> findAllByCourseIdAndIsActiveTrue(int courseId);
}
