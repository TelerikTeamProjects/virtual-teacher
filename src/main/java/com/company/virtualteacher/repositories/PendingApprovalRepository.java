package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.PendingApprovals;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PendingApprovalRepository extends JpaRepository<PendingApprovals, Integer> {
    List<PendingApprovals> findAllByUsername(String username);

    Page<PendingApprovals> findAllByUsernameContaining(String username, Pageable pageable);
}
