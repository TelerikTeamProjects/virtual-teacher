package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.CourseImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseImageRepository extends JpaRepository<CourseImage, Integer> {
    CourseImage findByCourseId(int courseId);
}
