package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "assignments")
public class Assignment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "file_name")
    @NotNull
    @Size(min = 5, max = 100, message = "File name should be between 5 and 100 characters.")
    private String fileName;

    @Column(name = "lecture_id")
    @NotNull
    private int lectureId;

    @Column(name = "file_type")
    @NotNull
    @Size(min = 3, max = 100, message = "File type should be between 3 and 100 characters.")
    private String fileType;

    @Column(name = "file")
    private byte[] file;

    public Assignment() {}

    public Assignment(int lectureId, String fileName, String fileType, byte[] file) {
        this.lectureId = lectureId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.file = file.clone();
    }

    public Assignment(int id, int lectureId, String fileName, String fileType, byte[] file) {
        this.id = id;
        this.lectureId = lectureId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.file = file.clone();
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLectureId() {
        return lectureId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getFile() {
        return file.clone();
    }

    public void setFile(byte[] file) {
        this.file = file.clone();
    }
}
