package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_lectures")
public class UserLecture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "lecture_id")
    @NotNull
    private int lectureId;

    @Column(name = "prev_lecture_id")
    private Integer prevLectureId;

    @Column(name = "next_lecture_id")
    private Integer nextLectureId;

    @Column(name = "course_id")
    @NotNull
    private int courseId;

    @Column(name = "student")
    @NotNull
    @Size(min = 5, max = 15, message = "Student name should be between 5 and 15 characters.")
    private String student;

    @Column(name = "video_watched")
    @NotNull
    private Boolean videoWatched;

    @Column(name = "homework_submitted")
    @NotNull
    private Boolean homeworkSubmitted;

    @Column(name = "available")
    @NotNull
    private Boolean available;

    public UserLecture() {}

    public UserLecture(int lectureId, Integer prevLectureId, Integer nextLectureId, int courseId, String student, Boolean videoWatched, Boolean homeworkSubmitted, Boolean available) {
        this.lectureId = lectureId;
        this.prevLectureId = prevLectureId;
        this.nextLectureId = nextLectureId;
        this.courseId = courseId;
        this.student = student;
        this.videoWatched = videoWatched;
        this.homeworkSubmitted = homeworkSubmitted;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLectureId() {
        return lectureId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Boolean isVideoWatched() {
        return videoWatched;
    }

    public void setVideoWatched(Boolean videoWatched) {
        this.videoWatched = videoWatched;
    }

    public Boolean isHomeworkSubmitted() {
        return homeworkSubmitted;
    }

    public void setHomeworkSubmitted(Boolean homeworkSubmitted) {
        this.homeworkSubmitted = homeworkSubmitted;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getPrevLectureId() {
        return prevLectureId;
    }

    public void setPrevLectureId(Integer prevLectureId) {
        this.prevLectureId = prevLectureId;
    }

    public Integer getNextLectureId() {
        return nextLectureId;
    }

    public void setNextLectureId(Integer nextLectureId) {
        this.nextLectureId = nextLectureId;
    }
}
