package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "title")
    @NotNull
    @Size(min = 5, max = 50, message = "Title should be between 5 and 25 characters.")
    private String title;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "description")
    @NotNull
    @Size(min = 5, max = 500, message = "Description should be between 5 and 500 characters.")
    private String description;

    @Column(name = "program")
    private String program;

    @Column(name = "teacher")
    @NotNull
    @Size(min = 5, max = 500, message = "Teacher nae should be between 5 and 500 characters.")
    private String teacher;

    @Column(name = "avg_rating")
    private double avgRating;

    @Column(name = "status_id")
    private int status;

    @OneToOne
    @JoinColumn(name = "image_id")
    private CourseImage image;

    public Course() {}

    public Course(int id, String title, Category category, String description, String program, String teacher,
                  double avgRating, int status, CourseImage image) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.description = description;
        this.program = program;
        this.teacher = teacher;
        this.avgRating = avgRating;
        this.status = status;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CourseImage getImage() {
        return image;
    }

    public void setImage(CourseImage image) {
        this.image = image;
    }
}
