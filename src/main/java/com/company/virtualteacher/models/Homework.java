package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "homeworks")
public class Homework {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "file_name")
    @NotNull
    @Size(min = 5, max = 100, message = "File name should be between 5 and 100 characters.")
    private String fileName;

    @Column(name = "student")
    @NotNull
    @Size(min = 5, max = 15, message = "Student name should be between 5 and 25 characters.")
    private String student;

    @Column(name = "course_id")
    @NotNull
    private int courseId;

    @Column(name = "lecture_id")
    @NotNull
    private int lectureId;

    @Column(name = "grade")
    @DecimalMax("6.0") @DecimalMin("2.0")
    private Double grade;

    @Column(name = "type")
    @NotNull
    @Size(min = 3, max = 100, message = "File extension should be between 3 and 100 characters.")
    private String fileType;

    @Column(name = "file")
    private byte[] file;

    public Homework() {
    }

    public Homework(String fileName, String student, int courseId, int lectureId, String fileType, byte[] file) {
        this.fileName = fileName;
        this.student = student;
        this.courseId = courseId;
        this.lectureId = lectureId;
        this.fileType = fileType;
        this.file = file.clone();
    }

    public Homework(int id, String fileName, String student, int courseId, int lectureId, Double grade, String fileType, byte[] file) {
        this.id = id;
        this.fileName = fileName;
        this.student = student;
        this.courseId = courseId;
        this.lectureId = lectureId;
        this.grade = grade;
        this.fileType = fileType;
        this.file = file.clone();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getLectureId() {
        return lectureId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getFile() {
        return file.clone();
    }

    public void setFile(byte[] file) {
        this.file = file.clone();
    }
}
