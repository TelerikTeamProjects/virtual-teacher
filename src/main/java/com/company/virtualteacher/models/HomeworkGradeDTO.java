package com.company.virtualteacher.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class HomeworkGradeDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "student")
    @NotNull
    @Size(min = 5, max = 15, message = "Student name should be between 5 and 25 characters.")
    private String student;

    @Column(name = "course_id")
    @NotNull
    private int courseId;

    @Column(name = "lecture_id")
    @NotNull
    private int lectureId;

    @Column(name = "grade")
    @DecimalMax("6.0") @DecimalMin("2.0")
    private Double grade;

    public HomeworkGradeDTO() {
    }

    public HomeworkGradeDTO(int id, String student, int courseId, int lectureId, Double grade) {
        this.id = id;
        this.student = student;
        this.courseId = courseId;
        this.lectureId = lectureId;
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getLectureId() {
        return lectureId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }
}
