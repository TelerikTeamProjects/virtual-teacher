'use strict';

loadCategories();

$(document).ready(function () {
    $(document).on('click', 'a.page-link', function (event) {
        if (!$(this).parent().hasClass('active')) {
            loadCategories($(this).data('value'))
            event.preventDefault();
        }
    });

    $(document).on('click', '.category', function () {
        let categoryId = $(this).attr("id");
        console.log(categoryId);
        let categoryName = $(this).attr("name");
        console.log(categoryName);

        let jsonString = JSON.stringify({
            id: categoryId,
            name: categoryName
        });
        // console.log(jsonString);

        $.ajax({
            type: 'POST',
            url: "/courses",
            contentType: 'application/json',
            data: jsonString, // access in body
        }).done(function (response) {
            //console.log('SUCCESS');
            //console.log(response)
            window.location.href = "/courses"
        }).fail(function (msg) {
            console.log('FAIL');
        }).always(function (msg) {
            console.log('ALWAYS');
        });
    });
});

function loadCategories(page) {
    // console.log('test');
    if (page === undefined) {
        page = 0;
    } else {
        page = Number(page) - 1;
    }

    // console.log(page);
    $.ajax({
        url: "/api/courses/groupbycategory?page=" + page,
        success: function (response) {
            if (response.content.length === 0) {
                $('#coursesByCategory').append(`<h2>No courses found</h2>`);
                $('.pagination').html("");

            } else {
                $('.pagination').html("");
                $('.pagination').append(`<li class="page-item prev"><a data-value="${response.pageable.pageNumber}" class="page-link" href="#coursesByCategory" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a></li>`);

                for (let i = 1; i <= response.totalPages; i++) {
                    $('.pagination').append(`<li class="page-item page-item-${i}"><a data-value="${i}" class="page-link" href="">${i}</a></li>`);
                    if (i === response.pageable.pageNumber + 1) {
                        $('.page-item-' + String(i)).addClass('active');
                    }
                }

                $('.pagination').append(`<li class="page-item next"><a data-value="${response.pageable.pageNumber + 2}" class="page-link" href="" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a></li>`);

                if (response.pageable.pageNumber === 0) {
                    $('.prev').addClass('disabled');
                }
                if (response.pageable.pageNumber + 1 === response.totalPages) {
                    $('.next').addClass('disabled');
                }

                $('#coursesByCategory').html("");
                $.each(response.content, function (i) {
                    let image;
                    if (response.content[i].categoryImage !== null) {
                        image = 'data:image/png;base64,' + response.content[i].categoryImage;
                    } else {
                        image = '/img/categories/category-default-img.jpg';
                    }

                    $('#coursesByCategory')
                        .append(`
                    <!-- category -->
            <div class="col-lg-4 col-md-6 category" name="${response.content[i].categoryName}" id="${response.content[i].categoryId}">
                <div class="category-item">
                    <div class="ci-thumb set-category-bg" data-setbg="${image}"></div>
                    <div class="ci-text">
                        <h5>${response.content[i].categoryName}</h5>
                        <span>${response.content[i].count} Courses</span>
                    </div>
                </div>
            </div>`)

                    /*------------------
                    Background Set
                    --------------------*/
                    $('.ci-thumb.set-category-bg:last').each(function () {
                        let bg = $(this).data('setbg');
                        // console.log(bg);
                        $(this).css('background-image', 'url(' + bg + ')');
                    });
                })
            }
        }
    })
}
