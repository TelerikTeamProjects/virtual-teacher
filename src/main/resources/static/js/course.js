'use strict';

loadLectures();

let description = $('#description-div').text();
let program = $('#program-div').html();

$(document).ready(function () {
    let prEdit;
    let descEdit;

    $(document).on('click', '#enroll', function () {
        enrollStudent();
    });

    $(document).on('click', '#edit-program-btn', function () {
        if (!prEdit) {
            prEdit = new nicEditor({fullPanel: true}).panelInstance('program-div', {hasPanel: true});
            $('#edit-program-btn').text('Submit');
            $('#cancel-program-btn').prop({hidden: '', disabled: ''});
        }
        else {
            prEdit.removeInstance('program-div');
            prEdit = null;
            $('#edit-program-btn').text('Edit Program');
            $('#cancel-program-btn').prop({hidden: 'true', disabled: 'true'});
            updateCourse('program');
        }
    });

    $(document).on('click', '#cancel-program-btn', function () {
        prEdit.removeInstance('program-div');
        prEdit = null;
        $('#edit-program-btn').text('Edit Program');
        $('#cancel-program-btn').prop({hidden: 'true', disabled: 'true'});
        $('#program-div').html(program);
    });

    $(document).on('click', '#edit-description-btn', function () {
        if (!descEdit) {
            $('#description-div').html(`<textarea id="course-description-text"></textarea>`);
            $('#course-description-text').val(description);
            $('#course-description-text').height( $('#course-description-text')[0].scrollHeight );
            descEdit = 'edit';
            $('#edit-description-btn').text('Submit');
            $('#cancel-description-btn').prop({hidden: '', disabled: ''});
        }
        else {
            descEdit = null;
            $('#edit-description-btn').text('Edit Description');
            $('#cancel-description-btn').prop({hidden: 'true', disabled: 'true'});
            updateCourse('description');
            $('#description-div').text(description);
        }
    });

    $(document).on('click', '#cancel-description-btn', function () {
        descEdit = null;
        $('#edit-description-btn').text('Edit Description');
        $('#cancel-description-btn').prop({hidden: 'true', disabled: 'true'});
        $('#course-description-text').remove();
        $('#description-div').text(description);
    });

    $(document).on('click', '#to-lectures', function () {
        $('html, body').animate({
            scrollTop: $("#lectures-container").offset().top
        }, 500);
    });
});

function loadLectures() {
    $.ajax({
        url: 'http://localhost:8080/api/courses/' + courseId +'/lectures/',
        success: function (response) {
            $.each(response, function (i, v) {
                $('#lectures-container').append(`
                <hr>
                <div class="row lecture-row">
                    <div class="col-lg-3">
                        <div class="video">
                            <img src="https://img.youtube.com/vi/${response[i].videoURL}/mqdefault.jpg" class="img-thumbnail" alt="${response[i].title}"/>
                            <a class="lecture-link" id="${i + '_link_' + response[i].id}" href="#" onclick="return false;"></a>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <h4 class="lecture-title">${response[i].title}</h4>
                        <h7 class="lecture-duration" id="${i + '_' + response[i].videoURL}"></h7>
                        <div class="lecture-description">${response[i].description}</div>
                        <div class="row" id="${i + '_progress_' + response[i].id}"></div>
                    </div>
                    <div class="col-lg-1 justify-content-center btn-container">
                        <button type="button" class="btn btn-success lecture-btn"  id="${i + '_btn_' + response[i].id}" onclick="location.href='/courses/${courseId}/lectures/${response[i].id}'" hidden disabled>Watch</button>
                        <div class="grade-div" id="${i + '_grade_' + response[i].id}" hidden>Grade: </div>
                    </div>
                </div>
                <div class="row assignments-row" id="${i + '_assignments_' + response[i].id}"></div>
                `);
                $.getJSON('https://www.googleapis.com/youtube/v3/videos?id=' + response[i].videoURL + '&part=contentDetails&key=AIzaSyAuyx3_HeoIXiQDqUxAvVobVnCDGiD46Kw',function(data,status,xhr){
                    let dur = moment.duration(data.items[0].contentDetails.duration);
                    $('#' + i + '_' + response[i].videoURL).text('Time: ' + (dur.hours() > 0 ? (dur.hours() + ' hours ') : '') + (dur.minutes() > 0 ? (dur.minutes() + ' minutes ') : '') + dur.seconds() + ' seconds');
                });
            })
        },
        complete: function () {
            if (!isTeacher) {
                $.ajax({
                    url: 'http://localhost:8080/api/courses/' + courseId + '/enrolled?username=' + username,
                    success: function (response) {
                        if (response) {
                            $('#enroll-row').remove();
                            setLectures();
                            setProgress();
                        } else {
                            $('#enroll').prop({hidden: '', disabled: ''});
                        }
                    }
                });
            }
        }
    });
}

function enrollStudent() {
    let jsonString = JSON.stringify({
        "student": username,
        "course": {
            "id": courseId
        },
        "completed": false
    });

    $.ajax({
        type: "POST",
        url: 'http://localhost:8080/api/courses/' + courseId + '/enroll',
        data: jsonString,
        contentType: 'application/json',
        success: function () {
            $('#enroll-row').remove();
            setProgress();
            setLectures();
        }
    });
}

function setLectures() {
    $.ajax({
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/lectures?username=' + username,
        success: function (response) {
            $.each(response, function (i, v) {
                if (response[i].available) {
                    $('#' + i + '_link_' + response[i].lectureId).prop({'href': '/courses/' + courseId + '/lectures/' + response[i].lectureId, 'onclick': null});
                    $('#' + i + '_btn_' + response[i].lectureId).prop({hidden: '', disabled: ''});

                    $.ajax({
                        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/lectures/' + response[i].lectureId + '/progress?username=' + username,
                        success: function (data) {
                            let progress = 0;
                            if (data.videoWatched && data.homeworkSubmitted) {
                                progress = 100;
                            }
                            else if (data.videoWatched || data.homeworkSubmitted) {
                                progress = 50;
                            }
                            $('#' + i + '_progress_' + response[i].lectureId).append(`
                                <div class="col-lg-8">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: ${progress}%;" aria-valuenow="${progress}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <span>${progress}% completed</span>
                                </div>
                            `);
                        }
                    });

                    $.ajax({
                        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/lectures/' + response[i].lectureId + '/grade?username=' + username,
                        success: function (data) {
                            if (data > 0.0) {
                                $('#' + i + '_grade_' + response[i].lectureId).append(data.toFixed(2)).prop({hidden: ''});
                            }
                        }
                    });
                }
            });
            $.ajax({
                url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/grades/average/?username=' + username,
                success: function (response) {
                    $('.average-grade').append(response.toFixed(2));
                }
            });
        }
    });
}

function setProgress() {
    $.ajax({
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/progress/total/?username=' + username,
        success: function (response) {
            $('#progress-row').append(`
                <div class="col-lg-9">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: ${response}%;" aria-valuenow="${response}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <span>${response}% completed</span>
                </div>
            `);
            if (response === 100) {
                setRatingLogic();
            }
        }
    });
}

function updateCourse(updatedField) {
    if (updatedField === 'program') {
        program = $('#program-div').html();
    }
    else if (updatedField === 'description') {
        description = $('#course-description-text').val();
    }

    let jsonString = JSON.stringify({
        "id": courseId,
        "description": description,
        "program": program,
        "teacher": username
    });

    $.ajax({
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/update',
        type: "PUT",
        data: jsonString,
        contentType: 'application/json',
        success: function (response) {
            $('#' + updatedField + '-span').text('The ' + updatedField + ' was updated successfully.');
        }
    });
}

function setRatingLogic() {
    $.ajax({
        url: 'http://localhost:8080/api/courses/' + courseId + '/completed?username=' + username,
        success: function (response) {
            if (response) {
                $.ajax({
                    url: 'http://localhost:8080/api/courses/' + courseId + '/rating?username=' + username,
                    success: function (responseRating) {
                        if (responseRating > 0) {
                            $('.rating-text').text(`Your rating:`);
                            $('.rating-container').text(`${responseRating.toFixed(1)}`);
                        }
                        else {
                            $('.rating-text').text(`Rate course:`);
                            $('.rating-container').html(`
                    <section class='rating-widget'>
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                                <li class='star' title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    </section>`);

                            /* 1. Visualizing things on Hover - See next part for action on click */
                            $('#stars li').on('mouseover', function(){
                                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                                // Now highlight all the stars that's not after the current hovered star
                                $(this).parent().children('li.star').each(function(e){
                                    if (e < onStar) {
                                        $(this).addClass('hover');
                                    }
                                    else {
                                        $(this).removeClass('hover');
                                    }
                                });

                            }).on('mouseout', function(){
                                $(this).parent().children('li.star').each(function(e){
                                    $(this).removeClass('hover');
                                });
                            });


                            /* 2. Action to perform on click */
                            $('#stars li').on('click', function(){
                                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                                var stars = $(this).parent().children('li.star');

                                for (var i = 0; i < stars.length; i++) {
                                    $(stars[i]).removeClass('selected');
                                }

                                for (i = 0; i < onStar; i++) {
                                    $(stars[i]).addClass('selected');
                                }
                                rateCourse(onStar);
                            });
                        }
                    }
                });
            }
        }
    });
}

function rateCourse(rating) {
    let jsonString = JSON.stringify({
        "courseId": courseId,
        "username": username,
        "rating": rating
    });

    $.ajax({
        type: 'PUT',
        url: 'http://localhost:8080/api/courses/' + courseId + '/rate',
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            $('.rating-text').text(`Your rating:`);
            $('.rating-container').text(`${data.rating.toFixed(1)}`);
            updateRating();
        }
    });
}

function updateRating() {
    $.ajax({
        url: 'http://localhost:8080/api/courses/' + courseId + '/avgrating',
        success: function (response) {
            $('#course-rating').text('Rating: ' + response.toFixed(1));
        }
    });
}