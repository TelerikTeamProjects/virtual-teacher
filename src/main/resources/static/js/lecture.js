'use strict';

loadLecture();

$(document).ready(function () {
    if (prevLectureId != null) {
        $('.prev-container').append('<button type="button" class="btn btn-success" id="prev-btn">Previous</button>')
    }

    $(document).on('click', '#prev-btn', function () {
        window.location.href = 'http://localhost:8080/courses/' + courseId +'/lectures/' + prevLectureId;
    });

    $(document).on('click', '#back-btn', function () {
        window.location.href = 'http://localhost:8080/courses/' + courseId;
    })

    $(document).on('click', '#next-btn', function () {
        window.location.href = 'http://localhost:8080/courses/' + courseId +'/lectures/' + nextLectureId;
    });

    $(document).on('click', '#continue-btn', function () {
        setVideoStatus(true);
    });

    $(document).on('click', '#download-btn', function () {
        let href = $('#download-link').attr('href');
        window.location.href = href;
    });

    $(document).on('change', '#singleFileUploadInput', function () {
        $('#input-label-1').text($(this).val().replace(/([^\\]*\\)*/,''));
    });

    $(document).on('submit', '#singleUploadForm', function (event) {
        uploadFile(this);
        event.preventDefault();
    });

});

function loadLecture() {
    $.ajax({
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/lectures/' + lectureId + '/progress?username=' + username,
        success: function (response) {
            if (response.videoWatched) {
                loadAssignment(response.homeworkSubmitted);
            }
            else {
                $('#lecture_message').html(`
                    <h5>Please watch the full lecture before you continue forward to the assignment.</h5>
                    <button type="button" class="btn btn-success" id="continue-btn">Continue</button>
                `);
            }
        }
    });
}

function setVideoStatus(status) {
    let jsonString = JSON.stringify({
        "lectureId": lectureId,
        "courseId": courseId,
        "student": username,
        "videoWatched": status,
        "homeworkSubmitted": false,
        "available": true
    });

    $.ajax({///courses/{courseId}/lectures/{lectureId}/watched/set"
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/lectures/' + lectureId + '/watched/set',
        type: "PUT",
        data: jsonString,
        contentType: 'application/json',
        success: function (response) {
            $('#lecture_message').html('');
            loadAssignment();
        }
    });
}

function loadAssignment(homeworkSubmitted) {
    $.ajax({
        url: 'http://localhost:8080/api/lectures/' + lectureId + '/assignments/' + assignmentId + '/name',
        success: function (response) {
            $('.assignment-container').html(`
                <h4>Assignment:</h4>
                <a id="download-link" href="http://localhost:8080/api/lectures/${lectureId }/assignments/${assignmentId}/download">${response}</a><button type="button" class="btn btn-success" id="download-btn">Download</button>
                <div class="upload-content">
                    <h4>Homework:</h4>
                </div>
            `);

            if (homeworkSubmitted) {
                $('.upload-content').append(`<div>Home work is submitted.</div>`);
                if (nextLectureId != null) {
                    $('.next-container').append('<button type="button" class="btn btn-success" id="next-btn">Next</button>');
                }
            }
            else {
                $('.upload-content').append(`
                    <div class="single-upload">
                        <form id="singleUploadForm" name="singleUploadForm">
                            <label id="input-label-0" class="btn btn-success" for="singleFileUploadInput">Choose a file</label>
                            <label id="input-label-1" for="singleFileUploadInput">No file chosen</label>
                            <input style="overflow: hidden;" id="singleFileUploadInput" type="file" name="file" class="file-input" required/>
                            <button type="submit" class="btn btn-success" id="submit-btn">Submit Homework</button>
                        </form>
                        <div class="upload-response">
                        </div>
                    </div>
                `);
            }
        }
    });
}

function uploadFile(thisForm) {
    let formData = new FormData(thisForm);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId +'/lectures/' + lectureId + '/homework/upload?username=' + username,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            $('.single-upload').remove()
            $('.upload-content').append(`<div>File Uploaded Successfully.</div>`);
            if (nextLectureId != null) {
                $('.next-container').append('<button type="button" class="btn btn-success" id="next-btn">Next</button>')
            }
        },
        error: function (error) {
            $('#upload-response').html('<p>Some Error Occurred</p>');
        }
    });
}