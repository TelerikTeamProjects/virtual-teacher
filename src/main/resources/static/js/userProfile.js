'use strict';

loadUserDetails();
$('.details-link').css({'font-weight' : '1000', 'color' : '#41BC3F'});

$(document).ready(function () {

    $(document).on('click', '.details-link', function (event) {
        toggleSelected(this);
        loadUserDetails();
        event.preventDefault();
    });

    $(document).on('click', '.enrolled-link', function (event) {
        toggleSelected(this);
        loadEnrolledCourses();
        event.preventDefault();
    });

    $(document).on('click', '.my-courses-link', function (event) {
        toggleSelected(this);
        loadTeacherCourses();
        event.preventDefault();
    });

    $(document).on('click', '.create-link', function (event) {
        toggleSelected(this);
    });

    let readURL = function (input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('.default-image').attr('src', e.target.result);
            };
            if ($('.user-image').length > 0) {
                reader.onload = function (e) {
                    $('.user-image').attr('src', e.target.result);
                };
            }

            reader.readAsDataURL(input.files[0]);
        }
    };

    $(document).on('change', '.file-input', function () {
        $(this).parents('.singleUploadForm').find('.input-label-1').text($(this).val().replace(/([^\\]*\\)*/,''));
        readURL(this);
    });

    $(document).on('click', '#submit-btn-user-img', function () {
        let thisForm = $(this).parents('form').get(0);
        uploadFile(thisForm);
    });

    $(document).on('click', '#save-btn', function () {
        let emailLocal = $('#email').val();
        let firstNameLocal = $('#firstName').val();
        let middleNameLocal = $('#middleName').val();
        let lastNameLocal = $('#lastName').val();

        if (validateUserDetails(emailLocal, firstNameLocal, middleNameLocal, lastNameLocal)) {
            saveUserDetails(emailLocal, firstNameLocal, middleNameLocal, lastNameLocal);
        }
    });

    $(document).on('click', '.assignment-count', function (event) {
        let courseIdLocal = $(this).parents('.course-div').attr('id');
        let courseTitle = $(this).parents('.course-div').find('.course-title').text();

        loadCoursesHomework(courseIdLocal, courseTitle);
        event.preventDefault();
    });

    $(document).on('click', '.download-btn', function () {
        let href = $(this).siblings('.download-link').attr('href');
        window.location.href = href;
    });

    $(document).on('click', '.submit-grade-btn', function (event) {
        let hmId = $(this).parents('.data-row').attr('id');
        let grade = $(this).parents('.data-row').find('.grade').val();
        let studentLocal = $(this).parents('.data-row').find('.student-holder').text();
        let lectureIdLocal = $(this).parents('.homework-container').find('.lecture-title-homework').attr('id');
        let courseIdLocal = $(this).parents('.lectures-container-homework').attr('id');
        if (grade < 2 || grade > 6) {
            alert("The grade mush be between 2 and 6.")
        }
        else {
            gradeHomework(hmId, studentLocal, courseIdLocal, lectureIdLocal, grade, $(this).parents('.data-row'));
        }
    });

});

// TODO history.pushState update
function toggleSelected(thisElement) {
    $('.details-link').css({'font-weight' : '500', 'color' : 'black'});
    $('.enrolled-link').css({'font-weight' : '500', 'color' : 'black'});
    $('.my-courses-link').css({'font-weight' : '500', 'color' : 'black'});
    $('.create-link').css({'font-weight' : '500', 'color' : 'black'});

    $(thisElement).css({'font-weight' : '1000', 'color' : '#41BC3F'});
}

function loadUserDetails() {
    $('.display-container').html(`
            <form class="user-details-form">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Profile Details</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="field-header">Email:<span class="user-email-error error"></span></p>
                        <input type="email" id="email" name="email" placeholder="" class="form-control email"
                               value="${email}" minlength="5" maxlength="50" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="field-header">First Name:<span class="user-firstName-error error"></span></p>
                        <input type="text" id="firstName" name="firstName" placeholder=""
                               class="form-control first-name"
                               value="${firstName != null ? firstName : ''}" minlength="3" maxlength="25" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="field-header">Middle Name:<span class="user-middleName-error error"></span></p>
                        <input type="text" id="middleName" name="middleName" placeholder=""
                               class="form-control middle-name"
                               value="${middleName != null ? middleName : ''}" minlength="3" maxlength="25" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="field-header">Last Name:<span class="user-lastName-error error"></span></p>
                        <input type="text" id="lastName" name="lastName" placeholder="" class="form-control last-name"
                               value="${lastName != null ? lastName : ''}" minlength="3" maxlength="25" required>
                    </div>
                </div>
                <div class="row save-btn-row">
                    <div class="col-lg-12 save-user-details">
                        <button type="button" class="btn btn-success" id="save-btn">Save</button>
                        <button type="reset" class="btn btn-success" id="reset-btn">Cancel</button>
                    </div>
                    <div class="col-lg-12 save-details-success"></div>
                    <div class="col-lg-12 save-details-error"></div>
                </div>
            </form>
    `).hide().slideDown('slow');
}

function saveUserDetails(emailLocal, firstNameLocal, middleNameLocal, lastNameLocal) {

    let jsonString = JSON.stringify(        {
        "id": userId,
        "username": username,
        "email": emailLocal,
        "firstName": firstNameLocal,
        "middleName": middleNameLocal,
        "lastName": lastNameLocal
    });

    $.ajax({
        type: 'PUT',
        url: 'http://localhost:8080/api/users/' + username + '/details',
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            $('#user-email-error').text('');
            $('#user-firstName-error').text('');
            $('#user-middleName-error').text('');
            $('#user-lastName-error').text('');
            $('.save-details-error').text('');
            $('.save-details-success').text(`Your profile was update successfully.`);

            $('#email').val(data.email)[0].defaultValue = data.email;
            $('#firstName').val(data.firstName)[0].defaultValue = data.firstName;
            $('#middleName').val(data.middleName)[0].defaultValue = data.middleName;
            $('#lastName').val(data.lastName)[0].defaultValue = data.lastName;

            email = data.email;
            firstName = data.firstName;
            middleName = data.middleName;
            lastName = data.lastName;

            setTimeout(function(){
                $('.save-details-success').text('');
            }, 5000);
        },
        error: function (error) {
            $('.save-details-error').text("Some error occurred.").css('color', 'red');
        }
    });
}

function validateUserDetails(emailLocal, firstNameLocal, middleNameLocal, lastNameLocal) {
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let errorCount = 0;

    if (emailLocal.length < 5 || emailLocal.length > 50) {
        $('.user-email-error').text('The email should be between 5 and 50 characters.');
        errorCount += 1;
    }
    else if (!emailRegex.test(emailLocal)) {
        $('.user-email-error').text('Please enter a valid email address.');
        errorCount += 1;
    }
    else {
        $('.user-email-error').text('');
    }

    if (firstNameLocal.length < 3 || firstNameLocal.length > 25) {
        $('.user-firstName-error').text('The first name should be between 3 and 25 characters.');
        errorCount += 1;
    }
    else {
        $('.user-firstName-error').text('');
    }

    if (middleNameLocal.length > 0 && (middleNameLocal.length < 3 || middleNameLocal.length > 25)) {
        $('.user-middleName-error').text('The middle name should be between 3 and 25 characters or left blank.');
        errorCount += 1;
    }
    else {
        $('.user-middleName-error').text('');
    }

    if (lastNameLocal.length < 3 || lastNameLocal.length > 25) {
        $('.user-lastName-error').text('The last name should be between 3 and 25 characters.');
        errorCount += 1;
    }
    else {
        $('.user-lastName-error').text('');
    }

    return errorCount === 0;
}

function uploadFile(thisForm) {
    let formData = new FormData(thisForm);
    $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: 'http://localhost:8080/api/users/images/upload/' + username,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            $(thisForm).parents('.single-upload').find('.upload-response').text('File Uploaded Successfully.').css('color', 'black');
            $(thisForm).find('.input-label-1').text(response);
            setTimeout(function(){
                $(thisForm).parents('.single-upload').find('.upload-response').text('');
            }, 5000);
        },
        error: function (error) {
            $(thisForm).parents('.single-upload').find('.upload-response').text("Some error occurred.occurred").css('color', 'red');
        }
    });
}

function loadEnrolledCourses() {
    $('.display-container').html(`
            <div class="row">
                <div class="col-lg-12">
                    <h2>Enrolled Courses</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 id="ongoing">Ongoing: <span id="ongoing-count"></span></h3>
                    <br>
                </div>
            </div>
            <div class="row enrolled-courses">
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <hr><br>
                    <h3 id="completed">Completed: <span id="completed-count"></span></h3>
                    <br>
                </div>
            </div>
            <div class="row completed-courses">
            </div>
        `);

    $.ajax({
        url: 'http://localhost:8080/api/users/' + username + '/ongoingcourses',
        success: function (response) {
            if (response.length === 0) {
                $('.enrolled-courses').append(`<h5 class="no-courses">No Courses.</h5>`);
                $('#ongoing-count').text('0 courses');
            }
            else {
                $('#ongoing-count').text(response.length + (response.length === 1 ? ' course' : ' courses'));
                $.each(response, function (i) {
                    loadSingleCourse(response[i].course, $('.enrolled-courses'));
                });
            }
        }
    });

    $.ajax({
        url: 'http://localhost:8080/api/users/' + username + '/completedcourses',
        success: function (response) {
            if (response.length === 0) {
                $('.completed-courses').append(`<h5 class="no-courses">No Courses.</h5>`)
                $('#completed-count').text('0 courses');
            }
            else {
                $('#completed-count').text(response.length + (response.length === 1 ? ' course' : ' courses'));
                $.each(response, function (i) {
                    loadSingleCourse(response[i].course, $('.completed-courses'));
                });
            }
        }
    });
}

function loadTeacherCourses() {
    $('.display-container').html(`
            <div class="row">
                <div class="col-lg-12">
                    <h2>My Courses</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 id="active">Active: <span id="active-count"></span></h3>
                    <br>
                </div>
            </div>
            <div class="row active-courses">
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <hr><br>
                    <h3 id="drafts">Drafts: <span id="draft-count"></span></h3>
                    <br>
                </div>
            </div>
            <div class="row draft-courses">
            </div>
        `);

    $.ajax({
        url: 'http://localhost:8080/api/courses/' + username,
        success: function (response) {
            if (response.length === 0) {
                $('.active-courses').html(`<h5 class="no-courses">No Courses.</h5>`);
                $('#active-count').text('0 courses');
                $('.draft-courses').html(`<h5 class="no-courses">No Courses.</h5>`);
                $('#draft-count').text('0 courses');
            }
            else {
                let activeCount = 0;
                let draftCount = 0;
                $.each(response, function (i) {
                    if (response[i].status === 1) {
                        loadSingleTeacherCourse(response[i], $('.active-courses'), '');
                        activeCount += 1;
                    }
                    else if (response[i].status === 2) {
                        loadSingleTeacherCourse(response[i], $('.draft-courses'), 'drafts/');
                        draftCount += 1;
                    }
                });
                $('#active-count').text(activeCount + (activeCount=== 1 ? ' course' : ' courses'));
                if (activeCount === 0) {
                    $('.active-courses').html(`<h5 class="no-courses">No Courses.</h5>`);
                }
                $('#draft-count').text(draftCount + (draftCount=== 1 ? ' course' : ' courses'));
                if (draftCount === 0) {
                    $('.draft-courses').html(`<h5 class="no-courses">No Courses.</h5>`);
                }
            }
        }
    });
}

function loadSingleTeacherCourse(course, selector, link) {
    let str = (course.category.name).replace(/\s/g, '').toLowerCase();
    let image;
    if (course.image !== null) {
        image = 'data:image/png;base64,' + course.image.image;
    } else {
        image = '/img/categories/3.jpg';
    }

    selector.append(`
                    <!-- course -->
            <div class="mix col-lg-4 col-md-4 col-sm-6 ${str} course-div" id="${course.id}">
                <div class="course-item">
                    <a class="course-link" href="/courses/${link}${course.id}">
                        <div class="course-thumb set-course-bg" data-setbg="${image}">
                        <div class="rating">Rating: ${course.avgRating}</div>
                        </div>
                    </a>
                    <div class="course-info">
                        <div class="course-text">
                            <a class="course-link" href="/courses/${link}${course.id}">
                                <h5 class="course-title" title="${course.title}">${course.title}</h5>
                            </a>
                            <div class="student-number" id="student-number-${course.id}"></div>
                        </div>
                        <a class="assignments-link" href="#">
                            <div class="assignment-count" id="assignment-count-${course.id}"></div>
                        </a>
                    </div>
                </div>
            </div>`).hide().slideDown('slow');

    /*------------------
    Background Set
    --------------------*/
    selector.find('.course-thumb.set-course-bg:last').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    if (selector.is('.active-courses')) {
        $.ajax({
            url: 'http://localhost:8080/api/courses/' + course.id + '/enrolled/count',
            success: function (response) {
                selector.find('#student-number-' + course.id).text(response + (response === 1 ? ' Student  ' : ' Students ') + 'Enrolled');
            }
        });
        $.ajax({
            url: 'http://localhost:8080/api/enrolled/courses/' + course.id + '/assignments/count',
            success: function (response) {
                selector.find('#assignment-count-' + course.id).text(response + ' Homework for grading');
            }
        });
    }
    else {
        selector.find('.rating').hide();
    }
}

function loadSingleCourse(course, selector) {
    let str = (course.category.name).replace(/\s/g, '').toLowerCase();
    let image;
    if (course.image !== null) {
        image = 'data:image/png;base64,' + course.image.image;
    } else {
        image = '/img/categories/3.jpg';
    }

    selector.append(`
                    <!-- course -->
            <div class="mix col-lg-4 col-md-4 col-sm-6 ${str}">
            <a class="course-link" href="/courses/${course.id}">
                <div class="course-item">
                    <div class="course-thumb set-course-bg" data-setbg="${image}">
                    <div class="rating">Rating: ${course.avgRating}</div>
                    <div class="grade">Grade: </div>
                    </div>
                    <div class="progress-container"></div>
                    <div class="course-info">
                        <div class="course-text">
                            <h5 class="course-title" title="${course.title}">${course.title}</h5>
                            <p class="category-name" title="${course.category.name}">${course.category.name}</p>
                            <div class="description" title="${course.description}">${course.description}</div>
                        </div>
                        <div class="course-author">
                            <p>${course.teacher}</p>
                        </div>
                        <div class="course-rating">
                        </div>
                    </div>
                </div>
                </a>
                </div>`).hide().slideDown('slow');

    /*------------------
    Background Set
    --------------------*/
    selector.find('.course-thumb.set-course-bg:last').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    setProgress(selector.find('.progress-container:last'), course.id);
    setAvgGrade(selector.find('.grade:last'), course.id);
    if (selector.is('.completed-courses')) {
        setRating(selector.find('.course-rating:last'), course.id)
    }
}

function setProgress(selector, courseId) {
    $.ajax({
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/progress/total/?username=' + username,
        success: function (response) {
            selector.append(`
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: ${response}%;" aria-valuenow="${response}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="percentage">
                    <span>${response}% completed</span>
                </div>
            `);
        }
    });
}

function setAvgGrade(selector, courseId) {
    $.ajax({
        url: 'http://localhost:8080/api/enrolled/courses/' + courseId + '/grades/average/?username=' + username,
        success: function (response) {
            selector.append(response.toFixed(2));
        }
    });
}

function setRating(selector, courseId) {
    $.ajax({
        url: 'http://localhost:8080/api/courses/' + courseId + '/rating?username=' + username,
        success: function (response) {
            if (response > 0) {
                selector.text('Your rating: ' + response)
            }
            else {
                selector.text('Rate course')
            }
        }
    });
}

function loadCoursesHomework(courseIdLocal, courseTitle) {
    $('.display-container').html(`
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="course-title-homework">${courseTitle}</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 lectures-container-homework" id="${courseIdLocal}">
                </div>
            </div>
        `);

    $.ajax({
        url: 'http://localhost:8080/api/courses/' + courseIdLocal + '/lectures',
        success: function (response) {
            $.each(response, function (i) {
                $('.lectures-container-homework').append(`
                    <div class="row">
                        <div class="col-lg-12 homework-container" id="homework-container-${response[i].id}">
                            <h4 class="lecture-title-homework" id="${response[i].id}">${response[i].title}</h4>
                            <div class="row header-row">
                                <div class="col-lg-6">Homework file</div>
                                <div class="col-lg-3">Student</div>
                                <div class="col-lg-1">Grade</div>
                                <div class="col-lg-2"></div>
                            </div>
                        </div>
                    </div>
                `);

                $.ajax({
                    url: 'http://localhost:8080/api/enrolled/courses/' + courseIdLocal + '/lectures/' + response[i].id + '/homework/list',
                    success: function (responseHomework) {
                        if (responseHomework.length == 0) {
                            $('#homework-container-' + response[i].id).append(`
                                <div class="row data-row">
                                    <div class="col-lg-6">
                                        No Homework
                                    </div>
                                </div>
                            `);
                        }
                        $.each(responseHomework, function (j) {
                            $('#homework-container-' + response[i].id).append(`
                            <div class="row data-row" id="${responseHomework[j].id}">
                                <div class="col-lg-6">
                                    <a class="download-link" href="http://localhost:8080/api/enrolled/courses/${courseIdLocal}/lectures/${response[i].id}/homework/${responseHomework[j].id}/download">${responseHomework[j].fileName}</a>
                                    <button type="button" class="btn btn-success download-btn">Download</button>
                                </div>
                                <div class="col-lg-3 student-holder">${responseHomework[j].student}</div>
                                <div class="col-lg-1 garde"><input class="grade" type="number" min="2" max="6" value="0" step="0.25"></div>
                                <div class="col-lg-2"><button type="button" class="btn btn-success submit-grade-btn">Submit</button></div>
                            </div>
                            `);
                        });
                    }
                });
            });
        },
    });
}

function gradeHomework(id, studentLocal, courseIdLocal, lectureIdLocal, grade, selector) {
    let jsonString = JSON.stringify(        {
        "id": id,
        "student": studentLocal,
        "courseId": courseIdLocal,
        "lectureId": lectureIdLocal,
        "grade": grade
    });

    $.ajax({
        type: 'PUT',
        url: 'http://localhost:8080/api/enrolled/courses/' + courseIdLocal + '/lectures/' + lectureIdLocal + '/grade',
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            if (selector.parents('.homework-container').find('.data-row').length === 1) {
                selector.parents('.homework-container').append(`
                                <div class="row data-row">
                                    <div class="col-lg-6">
                                        No Homework
                                    </div>
                                </div>
                            `);
            }
            selector.remove();
        },
        error: function (error) {
            alert('An error occurred.');
        }
    });
}