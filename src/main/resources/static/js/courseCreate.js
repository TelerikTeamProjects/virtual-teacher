'use strict';

$(document).ready(function () {
    let prEdit = new nicEditor({fullPanel: true}).panelInstance('program-div', {hasPanel: true});

    autosize($('textarea'));

    $('select').selectpicker();

    if (courseId !== 0) {
        $('#save-course-btn').text('Update course details');
        loadLectures();
        $('#save-btn-container').show();
        $('#submit-course-btn').prop({hidden: '', disabled: ''});
        $('#delete-btn').prop({hidden: '', disabled: ''});
    }
    else {
        $('#course-img-upload').hide();
        $('.new-lecture-btn-container').hide();
        $('#save-btn-container').hide();
    }

    $(document).on('click', '#new-category-btn', function () {
        createCategory();
    });

    $(document).on('click', '#save-course-btn', function () {
        if (validateCourseInput()) {
            if (courseId === 0) {
                saveCourseDetails('http://localhost:8080/api/courses/new', 'POST', 'saved');
            }
            else {
                saveCourseDetails(`http://localhost:8080/api/courses/${courseId}/draft/update`, 'PUT', 'updated');
            }
        }
    });

    $(document).on('click', '.save-lecture-btn', function () {
        let container = $(this).parents('.lecture-row');
        let prevL = container.prev().prev().attr('id');
        let nextL = container.next().next().attr('id');
        let id = container.attr('id');
        let title = container.find('.lecture-title').val();
        let description = container.find('.lecture-description').val();
        let url = youtube_parser(container.find('.lecture-url').val());

        if (validateLectureInput(title, description, url, container)) {
            saveLecture(id, title, description, url, prevL, nextL, container);
        }
    });

    $(document).on('click', '.update-lecture-btn', function () {
        let container = $(this).parents('.lecture-row');
        let prevL = container.prev().prev().attr('id');
        let nextL = container.next().next().attr('id');
        let id = container.attr('id');
        let title = container.find('.lecture-title').val();
        let description = container.find('.lecture-description').val();
        let url = youtube_parser(container.find('.lecture-url').val());

        if (validateLectureInput(title, description, url, container)) {
            updateLecture(id, title, description, url, prevL, nextL, container);
        }
    });

    $(document).on('click', '.lecture-description', function () {
        autosize($('textarea'));
    });

    $(document).on('click', '.new-lecture-btn', function () {
        loadLectureTemplate();
    });


    $(document).on('click', '.cancel-lecture-btn', function () {
        let $removedRow = $(this).parents('.lecture-row');
        $removedRow.slideUp('slow', function() { $removedRow.remove(); } );
    });

    $(document).on('change', '.file-input', function () {
        $(this).parent('.singleUploadForm').find('.input-label-1').text($(this).val().replace(/([^\\]*\\)*/,''));
    });

    $(document).on('click', '.submit-btn-assignment', function (event) {
        let thisForm = $(this).parents('form').get(0);
        let lectureId = $(thisForm).parents('.lecture-row').attr('id');
        let url = 'http://localhost:8080/api/lectures/' + lectureId + '/assignments/upload';
        uploadFile(thisForm, url);
        event.preventDefault();
    });

    $(document).on('click', '#submit-btn-course-img', function () {
        let thisForm = $(this).parents('form').get(0);
        let url = 'http://localhost:8080/api/courses/' + courseId + '/images/upload?username=' + username;
        uploadFile(thisForm, url);
    });

    let readURL = function (input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('.default-image').attr('src', e.target.result);
            };
            if ($('.course-image').length > 0) {
                reader.onload = function (e) {
                    $('.course-image').attr('src', e.target.result);
                };
            }

            reader.readAsDataURL(input.files[0]);
        }
    };

    $(document).on('change', '#singleFileUploadInput-course', function () {
        $(this).parents('.single-upload').find('.input-label-1').text($(this).val().replace(/([^\\]*\\)*/,''));
        readURL(this);
    });


    $(document).on('click', '#submit-course-btn', function () {
        activateCourse();
    });

    $(document).on('click', '#delete-btn', function () {
        deleteCourse();
    });
});

function createCategory(){
    let name = prompt("Please enter category name.");
    let create = true;

    if (name != null){
        if (name.length < 3 || name.length > 25) {
            alert('Category name should be between 3 and 25 characters.');
            create = false;
            return;
        }
        $("#category option").each(function() {
            if ($(this).text().toLowerCase() === name.toLocaleLowerCase()) {
                alert('Category with name "' + name + '" already exists.');
                create = false;
                return;
            }
        });

        if (create) {
            let jsonString = JSON.stringify({
                "name": name,
            });

            $.ajax({
                url: 'http://localhost:8080/api/categories/new',
                type: "POST",
                data: jsonString,
                contentType: 'application/json',
                success: function (response) {
                    $('#category').append(`<option value="${response.id}">${response.name}</option>`)
                    $('#category').val(response.id);
                    $('.selectpicker').selectpicker('refresh');
                },
                error: function (error) {
                    alert(error.responseJSON.message);
                }
            });
        }
    }
}

function saveCourseDetails(url, type, message) {
    let title = $('#title').val();
    let categoryId = $('#category').val();
    let description = $('#course-description-text').val();
    let program = $('.nicEdit-main').html();

    let jsonString = JSON.stringify(        {
        "id": courseId,
        "title": title,
        "category": {
            "id": categoryId
        },
        "description": description,
        "program": program,
        "teacher": username
    });

    $.ajax({
        type: type,
        url: url,
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            $('#title-error').text('');
            $('#description-error').text('');
            $('#category-error').text('');
            $('.save-details-error').text('');
            $('.save-details-success').text(`Course details were ${message} successfully.`);

            setTimeout(function(){
                $('.save-details-success').text('');
                }, 5000);

            if (courseId === 0) {
                courseId = data.id;
                loadLectures();
            }
            $('#course-img-upload').show();
            history.pushState({
                id: 'course'
            }, 'New course', 'http://localhost:8080/courses/drafts/' + data.id);
            courseId = data.id;

            $('#save-btn-container').show();
            $('#submit-course-btn').prop({hidden: '', disabled: ''});
            $('#delete-btn').prop({hidden: '', disabled: ''});
        },
        error: function (error) {
            $('.save-details-error').text(error.responseJSON.message).css('color', 'red');
        }
    });
}

function validateCourseInput() {
    let title = $('#title').val();
    let categoryId = $('#category').val();
    let description = $('#course-description-text').val();

    let errorCount = 0;
    if (title.length < 5 || title.length > 25) {
        $('#title-error').text('The title should be between 5 and 25 characters.');
        errorCount += 1;
    }
    if (description.length < 5 || description.length > 500) {
        $('#description-error').text('The description should be between 5 and 500 characters.');
        errorCount += 1;
    }
    if (categoryId === '0') {
        $('#category-error').text('Please select a category.');
        errorCount += 1;
    }

    return errorCount === 0;
}

function saveLecture(id, title, description, url, prevL, nextL, container) {

    let jsonString = JSON.stringify({
        "id": id,
        "title": title,
        "description": description,
        "courseId": courseId,
        "prevLectureId": prevL,
        "nextLectureId": nextL,
        "videoURL": url,
        "active": 1
    });

    $.ajax({
        type: "POST",
        url: "http://localhost:8080/api/lectures/new",
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            container.find('.lecture-title-error').text('');
            container.find('.lecture-description-error').text('');
            container.find('.lecture-url-error').text('');
            container.find('.save-lecture-error').text('');
            container.find('.save-lecture-success').text('The lecture was saved successfully.');
            setTimeout(function(){
                $('.save-lecture-success').text('');
            }, 5000);
            container.attr('id', data.id);
            container.find('.singleUploadForm').attr({'id' : 'singleUploadForm-' + data.id, 'name' : 'singleUploadForm-' + data.id });
            container.find('.input-label-0').attr({'id' : 'input-label-0-' + data.id, 'for' : 'singleFileUploadInput-' + data.id });
            container.find('.input-label-0').prop({hidden: '', disabled: ''});
            container.find('.input-label-1').attr({'id' : 'input-label-1-' + data.id, 'for' : 'singleFileUploadInput-' + data.id });
            container.find('.input-label-1').prop({hidden: '', disabled: ''});
            container.find('.file-input').attr({'id' : 'singleFileUploadInput-' + data.id});
            container.find('.file-input').prop({hidden: '', disabled: ''});
            container.find('.submit-btn-assignment').attr({'id' : 'submit-btn-' + data.id});
            container.find('.submit-btn-assignment').prop({hidden: '', disabled: ''});
            container.find('.save-lecture-container').html(`
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-success update-lecture-btn">Update lecture</button>
                                </div>
                            </div>
                            `);
        },
        error: function (error) {
            container.find('.save-lecture-error').text(error.responseJSON.message);
            setTimeout(function(){
                container.find('.save-lecture-error').text('');
            }, 5000);
        }
    });
}

function updateLecture(id, title, description, url, prevL, nextL, container) {

    let jsonString = JSON.stringify({
        "id": id,
        "title": title,
        "description": description,
        "courseId": courseId,
        "prevLectureId": prevL,
        "nextLectureId": nextL,
        "videoURL": url,
        "active": 1
    });

    $.ajax({
        type: "PUT",
        url: "http://localhost:8080/api/lectures/" + id + "/update",
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            container.find('.lecture-title-error').text('');
            container.find('.lecture-description-error').text('');
            container.find('.lecture-url-error').text('');
            container.find('.save-lecture-error').text('');
            container.find('.save-lecture-success').text('The lecture was updated successfully.');
            setTimeout(function(){
                $('.save-lecture-success').text('');
            }, 5000);
        },
        error: function (error) {
            container.find('.save-lecture-error').text(error.responseJSON.message);
            setTimeout(function(){
                container.find('.save-lecture-error').text('');
            }, 5000);
        }
    });
}

function validateLectureInput(title, description, url, container) {
    let errorCount = 0;
    if (title.length < 5 || title.length > 50) {
        container.find('.lecture-title-error').text('The title should be between 5 and 50 characters.');
        errorCount += 1;
    }
    else {
        container.find('.lecture-title-error').text('');
    }

    if (description.length < 5 || description.length > 500) {
        container.find('.lecture-description-error').text('The description should be between 5 and 500 characters.');
        errorCount += 1;
    }
    else {
        container.find('.lecture-description-error').text('');
    }

    if (!url) {
        container.find('.lecture-url-error').text('Please enter a valid YouTube link.');
        errorCount += 1;
    }
    else {
        container.find('.lecture-url-error').text('');
    }

    return errorCount === 0;
}

function youtube_parser(url){
    let regexYouTube = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/;
    if (!url.match(regexYouTube)) {
        return false;
    }
    let regexId = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    let match = url.match(regexId);
    return (match&&match[2].length === 11) ? match[2] : false;
}

function loadLectures() {
    $.ajax({
        url: "http://localhost:8080/api/courses/" + courseId + "/lectures",
        success: function (response) {
            $('#lectures-container').html('');
            $('#lectures-container').append(`<h3>Lectures:</h3>`);
            $.each(response, function (i, v) {
                $('#lectures-container').append(`
                <div class="row lecture-row" id="${response[i].id}">
                    <div class="col-lg-9 lecture-col">
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Title:<span class="lecture-title-error error"></span></p>
                                <input type="text" class="lecture-title"
                                   title="Enter title"
                                   name="lecture title" value="${response[i].title}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Description:<span class="lecture-description-error error"></span></p>
                                <textarea class="lecture-description">${response[i].description}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>YouTube Link:<span class="lecture-url-error error"></span></p>
                                <input type="text" class="lecture-url"
                                   title="Enter YouTube link"
                                   name="lecture link" value="https://www.youtube.com/embed/${response[i].videoURL}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="single-upload">
                                    <form class="singleUploadForm" id="singleUploadForm-${response[i].id}" name="singleUploadForm-${response[i].id}">
                                        <label id="input-label-0-${response[i].id}" class="btn btn-success input-label-0" for="singleFileUploadInput-${response[i].id}">Choose a file</label>
                                        <label id="input-label-1-${response[i].id}" class="input-label-1" for="singleFileUploadInput-${response[i].id}">No file chosen</label>
                                        <input style="overflow: hidden;" id="singleFileUploadInput-${response[i].id}" type="file" name="file" class="file-input" required/>
                                        <button type="button" class="btn btn-success submit-btn-assignment" id="submit-btn-${response[i].id}">Submit Assignment</button>
                                        <span class="upload-response"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row message-row">
                            <div class="col-lg-12">
                                <div class="save-lecture-success"></div>
                                <div class="save-lecture-error"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 save-lecture-div">
                        <div class="container-fluid save-lecture-container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-success update-lecture-btn">Update lecture</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `);

                if (response[i].assignmentId !== null) {
                    $.ajax({
                        url: 'http://localhost:8080/api/lectures/' + response[i].id + '/assignments/' + response[i].assignmentId + '/name',
                        success: function (data) {
                            $('#input-label-1-' + response[i].id).text(data);
                        }
                    });
                }
            })
            $('.new-lecture-btn-container').html(`
                    <div>
                        <button type="button" class="btn btn-success new-lecture-btn">New lecture</button>
                    </div>
                `).show();
        }
    });
}

function loadLectureTemplate() {
    let $container = $('#lectures-container');
    let newRow =`
                <div class="row lecture-row">
                    <div class="col-lg-9 lecture-col">
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Title:<span class="lecture-title-error error"></span></p>
                                <input type="text" class="lecture-title"
                                   title="Enter title"
                                   name="lecture title">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Description:<span class="lecture-description-error error"></span></p>
                                <textarea class="lecture-description"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>YouTube Link:<span class="lecture-url-error error"></span></p>
                                <input type="text" class="lecture-url"
                                   title="Enter YouTube link"
                                   name="lecture link">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                 <form class="singleUploadForm" id="singleUploadForm" name="singleUploadForm">
                                    <div class="single-upload">
                                        <label id="input-label-0" class="btn btn-success input-label-0" for="singleFileUploadInput" hidden disabled>Choose a file</label>
                                        <label id="input-label-1" class="input-label-1" for="singleFileUploadInput" hidden disabled>No file chosen</label>
                                        <input style="overflow: hidden;" id="singleFileUploadInput"
                                                    type="file" name="file" class="file-input" required hidden disabled/>
                                        <button type="button" class="btn btn-success submit-btn-assignment" id="submit-btn" hidden disabled>Submit Assignment</button>
                                        <span class="upload-response"></span>
                                     </div>
                                 </form>
                            </div>
                        </div>
                        <div class="row message-row">
                            <div class="col-lg-12">
                                <div class="save-lecture-success"></div>
                                <div class="save-lecture-error"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 save-lecture-div">
                        <div class="container-fluid save-lecture-container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-success save-lecture-btn">Save lecture</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-success cancel-lecture-btn">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           `;
    $(newRow).appendTo($container).hide().slideDown('slow');
    $('html, body').animate({
        scrollTop: $container.find('.lecture-row:last').offset().top
    }, 500);
}

function uploadFile(thisForm, url) {
    let formData = new FormData(thisForm);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            $(thisForm).find('.upload-response').text('File Uploaded Successfully.').css('color', 'black');
            $(thisForm).find('.input-label-1').text(response);
            setTimeout(function(){
                $(thisForm).find('.upload-response').text('');
            }, 5000);
        },
        error: function (error) {
            $(thisForm).find('.upload-response').text(error.responseJSON.message).css('color', 'red');
        }
    });
}

function activateCourse() {
    $.ajax({
        type: 'PUT',
        url: 'http://localhost:8080/api/courses/' + courseId + '/activate',
        contentType: 'application/json',
        success: function (data) {
            window.location.href='/courses/' + courseId;
        },
        error: function (error) {
            $('.submit-course-error').text(error.responseJSON.message).css('color', 'red');
        }
    });
}

function deleteCourse() {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/api/courses/' + courseId + '/delete',
        contentType: 'application/json',
        success: function (data) {
            window.location.href='/';
        },
        error: function (error) {
            $('.submit-course-error').text(error.responseJSON.message).css('color', 'red');
        }
    });
}