'use strict';

loadTrendingGifs();

$(document).ready(function () {
    $('.hidden-username').hide();
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $(document).on('click', 'a', function (event) {
        if (!$(this).parent().hasClass('navbar-header')) {
            $('li > a').removeClass('active-menu').trigger('change');
            $(this).addClass('active-menu');

            $('h2').text($(this).text().trim());

            switch ($(this).text().trim()) {
                case 'Dashboard':
                    loadTrendingGifs();
                    event.preventDefault();
                    break;
                case 'Courses':
                    loadCourses();
                    event.preventDefault();
                    break;
                case 'Students':
                    loadStudents();
                    event.preventDefault();
                    break;
                case 'Teachers':
                    loadTeachers();
                    event.preventDefault();
                    break;
                case 'Pending Approvals':
                    loadPendingApprovals();
                    event.preventDefault();
                    break;
                case 'Create User':
                    createUser();
                    event.preventDefault();
                    break;
                case 'My Profile':
                    loadMyProfile();
                    event.preventDefault();
                    break;
                default:
                    loadTrendingGifs();
                    event.preventDefault();
            }
            event.preventDefault();
        } else {
            event.preventDefault();
        }
    });

    $(document).on('click', 'a.page-link', function (event) {
        if (!$(this).parent().hasClass('active')) {
            let items = null;
            if ($(this).hasClass('courses')) {
                items = 'Courses';
            } else if ($(this).hasClass('students')) {
                items = 'Students';
            } else if ($(this).hasClass('teachers')) {
                items = 'Teachers';
            } else {
                items = 'Pending Approvals';
            }

            switch (items) {
                case 'Courses':
                    loadCourses($(this).data('value'));
                    event.preventDefault();
                    break;
                case 'Students':
                    loadStudents($(this).data('value'));
                    event.preventDefault();
                    break;
                case 'Teachers':
                    loadTeachers($(this).data('value'));
                    event.preventDefault();
                    break;
                case 'Pending Approvals':
                    loadPendingApprovals($(this).data('value'));
                    event.preventDefault();
                    break;
                default:
                    loadTrendingGifs();
                    event.preventDefault();
            }
            event.preventDefault();
        } else {
            event.preventDefault();
        }

    });

    $(document).on('click', 'a.logout', function (event) {
        window.location.href = "/logout";
    });
});

function loadTrendingGifs() {
    $.ajax({
        url: 'https://api.giphy.com/v1/gifs/trending?api_key=tYShKI8e6mJydiIj2UGmgUdrtY1726OG&limit=3&rating=G',
        success: function (response) {
            $('#container').html('');
            $('#container').append(`<h3 class="text-center">Smile <i class="fa fa-smile-o"></i></h3>`);
            $.each(response.data, function (i) {
                $('#container').append(`<img class="img-fluid img-thumbnail" src="${response.data[i].images.original.url}"/>`);
                // console.log(response.data[i].slug);
            })
        }
    })

    $('.container-fluid').hide();
}

function loadCourses(page) {
    $('.container-fluid').show();

    if (page === undefined) {
        page = 0;
    } else {
        page = Number(page) - 1;
    }

    let $urlExtension;
    let title = $('#title').val();
    if (title === undefined) {
        $urlExtension = generateCourseUrlExtension("", page);
    } else {
        $urlExtension = generateCourseUrlExtension(title, page);
    }

    $.ajax({
        url: '/api/admin/courses' + $urlExtension,
        success: function (response) {
            $('#container').addClass('courses');
            $('#container').html('');
            $('#container').append(`<!-- Search form -->
                    <form class="form-inline md-form form-sm mt-5">
          <input id="title" class="form-control form-control-sm mr-3 w-75" type="text" placeholder="Search" aria-label="Search">
          <i class="fa fa-search active fa-2x" aria-hidden="true" onclick="loadCourses()"></i>
        </form>`);

            if (response.totalElements === 0) {
                $('#container').append(`<h3 class="no-data">No courses found by searched criteria</h3>`);
                $('.pagination').html("");

            } else {
                $('.pagination').html('');
                $('.pagination').append(`<li class="page-item prev navbar-header"><a data-value="${response.pageable.pageNumber}" class="page-link courses" href="" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a></li>`);

                for (let i = 1; i <= response.totalPages; i++) {
                    $('.pagination').append(`<li class="page-item page-item-${i} navbar-header"><a data-value="${i}" class="page-link courses" href="">${i}</a></li>`);
                    if (i === response.pageable.pageNumber + 1) {
                        $('.page-item-' + String(i)).addClass('active');
                    }
                }

                $('.pagination').append(`<li class="page-item next navbar-header"><a data-value="${response.pageable.pageNumber + 2}" class="page-link courses" href="" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a></li>`);

                if (response.pageable.pageNumber === 0) {
                    $('.prev').addClass('disabled');
                }

                if (response.pageable.pageNumber + 1 === response.totalPages) {
                    $('.next').addClass('disabled');
                }
            }

            $('#container').append(`<div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>`);

            $.each(response.content, function (i) {
                // console.log(response);
                let imageVar;
                if (response.content[i].image !== null) {
                    imageVar = 'data:image/png;base64,' + response.content[i].image.image;
                } else {
                    imageVar = '/img/admin.png';
                }

                let courseLectures = lecturesCountByCourseId(response.content[i].id);
                let courseStudents = studentsCountByCourseId(response.content[i].id);


                $('#container tbody').append(`<tr title="${response.content[i].description}">
                                            <td><img src="${imageVar}" class="img-thumbnail-user" alt="${response.content[i].title}"/></td>
                                            <td>${response.content[i].title}</td>                                     
                                            <td>${response.content[i].teacher}</td>                                     
                                            <td>${courseLectures} Lectures</td>                                     
                                            <td>${courseStudents} Students</td>                                     
                                            <td><div class="text-white">
                            <button type="button" class="btn btn-danger m-1 text-center" onclick="deleteCourse('${response.content[i].id}', 'courses')">
                                Delete
                            </button>                    
                    </div></td>
                                        </tr>`);

            });

            $('#container').append(`</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
                </div>`);

        }
    })
}

function loadStudents(page) {
    $('.container-fluid').show();

    if (page === undefined) {
        page = 0;
    } else {
        page = Number(page) - 1;
    }

    let $urlExtension;
    let username = $('#username').val();
    if (username === undefined) {
        $urlExtension = generateUrlExtension("", page);
    } else {
        $urlExtension = generateUrlExtension(username, page);
    }

    $.ajax({
        url: '/api/admin/users/students' + $urlExtension,
        success: function (response) {
            $('#container').html('');
            $('#container').append(`<!-- Search form -->
                    <form class="form-inline md-form form-sm mt-5">
          <input id="username" class="form-control form-control-sm mr-3 w-75" type="text" placeholder="Search" aria-label="Search">
          <i class="fa fa-search active fa-2x" aria-hidden="true" onclick="loadStudents()"></i>
        </form>`);

            if (response.totalElements === 0) {
                $('#container').append(`<h3 class="no-data">No students found by searched criteria</h3>`);
                $('.pagination').html("");

            } else {
                $('.pagination').html('');
                $('.pagination').append(`<li class="page-item prev navbar-header"><a data-value="${response.pageable.pageNumber}" class="page-link students" href="" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a></li>`);

                for (let i = 1; i <= response.totalPages; i++) {
                    $('.pagination').append(`<li class="page-item page-item-${i} navbar-header"><a data-value="${i}" class="page-link students" href="">${i}</a></li>`);
                    if (i === response.pageable.pageNumber + 1) {
                        $('.page-item-' + String(i)).addClass('active');
                    }
                }

                $('.pagination').append(`<li class="page-item next navbar-header"><a data-value="${response.pageable.pageNumber + 2}" class="page-link students" href="" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a></li>`);

                if (response.pageable.pageNumber === 0) {
                    $('.prev').addClass('disabled');
                }

                if (response.pageable.pageNumber + 1 === response.totalPages) {
                    $('.next').addClass('disabled');
                }
            }

            $('#container').append(`<div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>`);

            $.each(response.content, function (i) {
                // console.log(response);
                let imageVar;
                if (response.content[i].image !== null) {
                    imageVar = 'data:image/png;base64,' + response.content[i].image.image;
                } else {
                    imageVar = '/img/admin.png';
                }

                $('#container tbody').append(`<tr>
                                            <td><img src="${imageVar}" class="img-thumbnail-user" alt="${response.content[i].username}"/></td>
                                            <td>${response.content[i].username}</td>
                                            <td><div class="text-white">
                        <button type="button" id="${response.content[i].username}" class="btn btn-warning m-1 text-center" onclick="promoteAdmin('${response.content[i].username}', 'students')">
                            Admin
                        </button>
                    </div></td>
                                            <td><div class="text-white">
                            <button type="button" class="btn btn-danger m-1 text-center" onclick="deleteUser('${response.content[i].username}', 'students')">
                                Delete
                            </button>                    
                    </div></td>
                                        </tr>`);

            });

            $('#container').append(`</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
                </div>`);

        }
    })
}

function loadTeachers(page) {
    $('.container-fluid').show();

    if (page === undefined) {
        page = 0;
    } else {
        page = Number(page) - 1;
    }

    let $urlExtension;
    let username = $('#username').val();
    if (username === undefined) {
        $urlExtension = generateUrlExtension("", page);
    } else {
        $urlExtension = generateUrlExtension(username, page);
    }

    $.ajax({
        url: '/api/admin/users/teachers' + $urlExtension,
        success: function (response) {
            $('#container').html('');
            $('#container').append(`<!-- Search form -->
                    <form class="form-inline md-form form-sm mt-5">
          <input id="username" class="form-control form-control-sm mr-3 w-75" type="text" placeholder="Search" aria-label="Search">
          <i class="fa fa-search active fa-2x" aria-hidden="true" onclick="loadTeachers()"></i>
        </form>`);

            if (response.totalElements === 0) {
                $('#container').append(`<h3 class="no-data">No teachers found by searched criteria</h3>`);
                $('.pagination').html("");

            } else {
                $('.pagination').html('');
                $('.pagination').append(`<li class="page-item prev navbar-header"><a data-value="${response.pageable.pageNumber}" class="page-link teachers" href="" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a></li>`);

                for (let i = 1; i <= response.totalPages; i++) {
                    $('.pagination').append(`<li class="page-item page-item-${i} navbar-header"><a data-value="${i}" class="page-link teachers" href="">${i}</a></li>`);
                    if (i === response.pageable.pageNumber + 1) {
                        $('.page-item-' + String(i)).addClass('active');
                    }
                }

                $('.pagination').append(`<li class="page-item next navbar-header"><a data-value="${response.pageable.pageNumber + 2}" class="page-link teachers" href="" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a></li>`);

                if (response.pageable.pageNumber === 0) {
                    $('.prev').addClass('disabled');
                }

                if (response.pageable.pageNumber + 1 === response.totalPages) {
                    $('.next').addClass('disabled');
                }
            }

            $('#container').append(`<div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>`);

            $.each(response.content, function (i) {
                // console.log(response);
                let imageVar;
                if (response.content[i].image !== null) {
                    imageVar = 'data:image/png;base64,' + response.content[i].image.image;
                } else {
                    imageVar = '/img/admin.png';
                }

                $('#container tbody').append(`<tr>
                                            <td><img src="${imageVar}" class="img-thumbnail-user" alt="${response.content[i].username}"/></td>
                                            <td>${response.content[i].username}</td>
                                            <td><div class="text-white">
                        <button type="button" id="${response.content[i].username}" class="btn btn-warning m-1 text-center" onclick="promoteAdmin('${response.content[i].username}', 'teachers')">
                            Admin
                        </button>
                    </div></td>
                                            <td><div class="text-white">
                            <button type="button" class="btn btn-danger m-1 text-center" onclick="deleteUser('${response.content[i].username}', 'teachers')">
                                Delete
                            </button>                    
                    </div></td>
                                        </tr>`);

            });

            $('#container').append(`</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
                </div>`);

        }
    })
}

function loadPendingApprovals(page) {
    $('.container-fluid').show();

    if (page === undefined) {
        page = 0;
    } else {
        page = Number(page) - 1;
    }

    let $urlExtension;
    let username = $('#username').val();
    if (username === undefined) {
        $urlExtension = generateUrlExtension("", page);
    } else {
        $urlExtension = generateUrlExtension(username, page);
    }

    $.ajax({
        url: '/api/admin/pendingapprovals' + $urlExtension,
        success: function (response) {
            $('#container').html('');
            $('#container').append(`<!-- Search form -->
                    <form class="form-inline md-form form-sm mt-5">
          <input id="username" class="form-control form-control-sm mr-3 w-75" type="text" placeholder="Search" aria-label="Search">
          <i class="fa fa-search active fa-2x" aria-hidden="true" onclick="loadPendingApprovals()"></i>
        </form>`);

            if (response.totalElements === 0) {
                $('#container').append(`<h3 class="no-data">No pending approvals found by searched criteria</h3>`);
                $('.pagination').html("");

            } else {
                $('.pagination').html('');
                $('.pagination').append(`<li class="page-item prev navbar-header"><a data-value="${response.pageable.pageNumber}" class="page-link approvals" href="" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a></li>`);

                for (let i = 1; i <= response.totalPages; i++) {
                    $('.pagination').append(`<li class="page-item page-item-${i} navbar-header"><a data-value="${i}" class="page-link approvals" href="">${i}</a></li>`);
                    if (i === response.pageable.pageNumber + 1) {
                        $('.page-item-' + String(i)).addClass('active');
                    }
                }

                $('.pagination').append(`<li class="page-item next navbar-header"><a data-value="${response.pageable.pageNumber + 2}" class="page-link approvals" href="" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a></li>`);

                if (response.pageable.pageNumber === 0) {
                    $('.prev').addClass('disabled');
                }

                if (response.pageable.pageNumber + 1 === response.totalPages) {
                    $('.next').addClass('disabled');
                }
            }

            $('#container').append(`<div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>`);

            $.each(response.content, function (i) {
                // console.log(response);
                $('#container tbody').append(`<tr>
                                            <td>${response.content[i].username}</td>
                                            <td><textarea cols="80">${response.content[i].description}</textarea></td>
                                            <td><div class="text-white">
                        <button type="button" id="${response.content[i].username}" class="btn btn-success m-1 text-center" onclick="approveTeacher('${response.content[i].username}','approvals')">
                            Approve
                        </button>
                    </div></td>                                        
                                        </tr>`);

            });

            $('#container').append(`</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
                </div>`);

        }
    })
}

function createUser() {
    $('.container-fluid').hide();
    $('#container').html('');
    $('#container').append(`
            <form id="userRegisterForm" name="userRegisterForm" 
            class="form" method="post" action="" onsubmit="return checkPassword(this);">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="username">Username</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                <input type="text" id="username" name="username" placeholder="" class="form-control"
                                       value="" minlength="5" maxlength="15" required>
                            </div>
                        </div>
                    </div>
                    </div>
        

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="email">Email</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-envelope"></i></div>
                                <input type="email" id="email" name="email" placeholder="" class="form-control"
                                       value="" minlength="5" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    </div>

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="password">Password</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-danger">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                                <input type="password" id="password" name="password" placeholder="" class="form-control"
                                       minlength="8" maxlength="64" required>
                            </div>
                        </div>
                        </div>
                
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="password_confirm">Confirm password</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-danger">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-repeat"></i></div>
                                <input type="password" id="password_confirm" name="password_confirm" placeholder=""
                                       class="form-control" minlength="8" maxlength="64" required>
                            </div>
                        </div>
                    </div>
                    </div>
                
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <button id="register" type="button" class="btn btn-success" onclick="createAdmin()">
                        <i class="fa fa-user-plus"></i> Register</button>
                    </div>
                </div>
            </form>`);

    $('#container').append(`<h5 class="response-data"></h5>`);
}

function loadMyProfile() {
    $('.container-fluid').hide();

    let username = $('.hidden-username').text();
    $('input[name="username"]').val('');
    $('input[name="email"]').val('');
    $('input[name="password"]').val('');
    $('input[name="password_confirm"]').val('');

    $.ajax({
        url: '/api/users/' + username,
        success: function (response) {
            $('#container').html('');
            $('#container').append(`
            <form id="userUpdateForm" name="userUpdateForm" 
            class="form" method="post" action="">
            
                <div class="row">
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="firstName">First Name</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input type="text" id="firstName" name="firstName" placeholder="${response.firstName}" 
                                value="${response.firstName != null ? response.firstName : ''}" class="form-control">
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="row">
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="middleName">Middle Name</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input type="text" id="middleName" name="middleName" placeholder="${response.middleName}" 
                                value="${response.middleName != null ? response.middleName : ''}" class="form-control">
                            </div>
                        </div>
                    </div>
                    </div>
                    
                     <div class="row">
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="lastName">Last Name</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input type="text" id="lastName" name="lastName" placeholder="${response.lastName}" 
                                value="${response.lastName != null ? response.lastName : ''}" class="form-control">
                            </div>
                        </div>
                    </div>
                    </div>
        
                <div class="row">
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="email">Email</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input type="email" id="email" name="email" placeholder="${response.email}" 
                                 value="${response.email}" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="row">
                    <div class="col-md-3 field-label-responsive text-left">
                        <label for="username">Username</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input id="username" name="username" placeholder="${response.username}" 
                                value="${response.username}" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    </div>
                                                             
                <div class="row">
                    <div class="col-md-6">
                        <button id="update" type="button" class="btn btn-success" onclick="updateProfile('${response.username}')">
                        <i class="fa fa-user-plus"></i> Update</button>
                    </div>
                </div>
            </form>`);

            $('#container').append(`<h5 class="response-data"></h5>`);
        }
    });

}

function promoteAdmin(username, role) {
    $.ajax({
        type: "PUT",
        url: '/api/admin/users/' + username + '/promote/admin',
        success: function (data) {
            console.log('success');

            updateCounters();
            reloadItems(role);
        }
    });
}

function deleteUser(username, role) {
    $.ajax({
        type: "DELETE",
        url: '/api/admin/users/' + username,
        success: function (data) {
            console.log('success');

            updateCounters();
            reloadItems(role);
        }
    });
}

function deleteCourse(id, item) {
    $.ajax({
        type: "DELETE",
        url: '/api/admin/courses/' + id + '/delete',
        success: function (data) {
            console.log('success');

            updateCounters();
            reloadItems(item);
        }
    });
}

function generateUrlExtension(username, page) {
    let url = "?";

    if (username !== "") {
        url = url + "username=" + username + "&";
    }

    url = url + "page=" + page;
    return url;
}

function generateCourseUrlExtension(title, page) {
    let url = "?";

    if (title !== "") {
        url = url + "title=" + title + "&";
    }

    url = url + "page=" + page;
    return url;
}

function updateCounters() {
    $.ajax({
            type: "GET",
            url: 'api/users/students/count',
            success: function (response) {
                $('.students').text(response + ' Students');
            }
        }
    );

    $.ajax({
            type: "GET",
            url: 'api/users/teachers/count',
            success: function (response) {
                $('.teachers').text(response + ' Teachers');
            }
        }
    );

    $.ajax({
            type: "GET",
            url: 'api/courses/count',
            success: function (response) {
                $('.courses').text(response + ' Courses');
            }
        }
    );
}

function reloadItems(item) {
    switch (item) {
        case 'courses':
            loadCourses();
            break;
        case 'students':
            loadStudents();
            break;
        case 'teachers':
            loadTeachers();
            break;
        case 'approvals':
            loadPendingApprovals();
            break;
        default:
            loadTrendingGifs();
    }
}

function lecturesCountByCourseId(id) {
    let lectures = 0;
    $.ajax({
            type: "GET",
            url: 'api/courses/' + id + '/lectures',
            async: false,
            success: function (response) {
                lectures = Object.keys(response).length;
            }
        }
    );

    return lectures;
}

function studentsCountByCourseId(id) {
    let students = 0;
    $.ajax({
            type: "GET",
            url: 'api/admin/courses/' + id + '/students',
            async: false,
            success: function (response) {
                students = response;

            }
        }
    );
    return students;
}

function approveTeacher(username, item) {
    $.ajax({
        type: "PUT",
        url: 'api/admin/users/' + username + '/promote/teacher',
        success: function (data) {
            console.log('success');

            updateCounters();
            reloadItems(item);
        },
        error: function (error) {
            $('#error-response').html('<p>Some Error Occurred</p>');
        }
    });
}

function createAdmin() {
    let username = $('input[name="username"]').val();
    let email = $('input[name="email"]').val();
    let password = $('input[name="password"]').val();

    let jsonString = JSON.stringify({
        username: username,
        email: email,
        password: password
    });
    // console.log(jsonString);

    $.ajax({
        type: 'POST',
        url: '/api/admin/new',
        contentType: 'application/json',
        data: jsonString,
        success: function (response) {
            $('input[name="username"]').val('');
            $('input[name="email"]').val('');
            $('input[name="password"]').val('');
            $('input[name="password_confirm"]').val('');
            $('.response-data').text('User is created successfully :)');
        },
        error: (function (response) {
                // let jsonResponseText = $.parseJSON(response.responseText);
                // $('.response-data').text(jsonResponseText.message);
               /* $('input[name="username"]').val('');
                $('input[name="email"]').val('');
                $('input[name="password"]').val('');
                $('input[name="password_confirm"]').val('');*/
                $('.response-data').text("Something went wrong.");
                // console.log('FAIL');
            }
        )
    });
}

function updateProfile(user) {
    let firstName = $('input[name="firstName"]').val();
    let middleName = $('input[name="middleName"]').val();
    let lastName = $('input[name="lastName"]').val();
    let email = $('input[name="email"]').val();
    let username = $('input[name="username"]').val();

    let jsonString = JSON.stringify({
        firstName: firstName,
        middleName: middleName,
        lastName: lastName,
        email: email,
        username: username
    });
    // console.log(jsonString);

    $.ajax({
        type: 'PUT',
        url: '/api/users/' + user + '/details',
        contentType: 'application/json',
        data: jsonString,
        success: function (response) {
            loadMyProfile();
            console.log('SUCCESS');
            // console.log(response)
        },
        error: (function (response) {
                // console.log(response);
                // let jsonResponseText = $.parseJSON(response.responseText);
                //$('.response-data').text(jsonResponseText.message);
                $('.response-data').text("Something went wrong.");
                // console.log('FAIL');
            }
        )
    });
}