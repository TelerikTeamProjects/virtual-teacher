package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.*;
import com.company.virtualteacher.repositories.UserCourseRepository;
import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.UserPresentationServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class UserPresentationServiceImplTests {
    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private UserCourseRepository mockUserCourseRepository;

    @InjectMocks
    private UserPresentationServiceImpl userPresentationService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Category newCategory = new Category(1, "Category Name", image);
    Course newCourse = new Course(1, "Course Title", newCategory, "Description", "Program",
            "didig", 1.5, 1, null);

    @Test
    public void getOngoingCourses_Should_ReturnCourseList_When_UserHasEnrolledCourses() {
        // Arrange
        Mockito.when(mockUserCourseRepository.findAllByStudentAndIsCompletedIsFalse("didig"))
                .thenReturn(Arrays.asList(
                        new UserCourse(1, "didig", newCourse, false)
                ));

        // Act
        List<UserCourse> result = userPresentationService.getOngoingCourses("didig");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getCompletedCourses_Should_ReturnCourseList_When_UserHasCompletedCourses() {
        // Arrange
        Mockito.when(mockUserCourseRepository.findAllByStudentAndIsCompletedIsTrue("didig"))
                .thenReturn(Arrays.asList(
                        new UserCourse(1, "didig", newCourse, true)
                ));

        // Act
        List<UserCourse> result = userPresentationService.getCompletedCourses("didig");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getTeachers_Should_ReturnTeachersList_When_TeacherStatusIsEnabled() {
        // Arrange
        List<String> teachers = new ArrayList<>(Arrays.asList("vasko", "didig", "teacher"));

        Mockito.when(mockUserRepository.getTeachers())
                .thenReturn(teachers);

        // Act
        List<String> result = userPresentationService.getTeachers();

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getUsers_Should_ReturnUsersList_When_FilterNotExists() {
        // Arrange
        Mockito.when(mockUserRepository.findAllByEnabledIsTrue())
                .thenReturn(Arrays.asList(
                        new User(1, "username1", "user@email.com", "First Name",
                                "Middle Name", "Last Name", true, null),
                        new User(2, "username2", "user@email.com", "First Name",
                                "Middle Name", "Last Name", true, null),
                        new User(3, "username3", "user@email.com", "First Name",
                                "Middle Name", "Last Name", true, null)
                ));

        // Act
        List<User> result = userPresentationService.findAll();

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getStudents_Should_Return_ListOfStudentsNames_When_MethodIsInvoked() {
        // Arrange
        List<String> studentsList = new ArrayList<>();
        studentsList.add("vasko");
        studentsList.add("didi");

        Mockito.when(mockUserRepository.getStudents())
                .thenReturn(studentsList);

        // Act
        List<String> result = userPresentationService.getStudents();

        // Assert
        Assert.assertEquals(result.size(), studentsList.size());
    }

    @Test
    public void getStudentsList_Shpould_ReturnPageOfStudentsDTO_When_MethodIsInvoked() {
        // Arrange
        List<User> users = new ArrayList<>();
        users.add(new User(1, "username1", "user@email.com", "First Name",
                "Middle Name", "Last Name", true, null));

        Page<User> page = new PageImpl<>(users);

        Mockito.when(mockUserRepository.getStudentsList(null))
                .thenReturn(page);

        // Act
        Page<UserManagementDTO> result = userPresentationService.getStudentsList(any());

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }

    @Test
    public void getUsersListByUsernameAndRole_Should_ReturnUsersList_When_FiltersNotExist(){
        List<User> users = new ArrayList<>();
        users.add(new User(1, "username1", "user@email.com", "First Name",
                "Middle Name", "Last Name", true, null));

        Page<User> page = new PageImpl<>(users);

        Mockito.when(mockUserRepository.getUsersListByUsernameAndRole("", "", null))
                .thenReturn(page);

        // Act
        Page<UserManagementDTO> result = userPresentationService.getUsersListByUsernameAndRole(any(), any(),  any());

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }
}
