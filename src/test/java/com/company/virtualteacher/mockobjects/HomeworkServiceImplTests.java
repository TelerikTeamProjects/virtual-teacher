package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.*;
import com.company.virtualteacher.repositories.*;
import com.company.virtualteacher.services.HomeworkServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.company.virtualteacher.services.CourseStatus.STATUS_ACTIVE;

@RunWith(MockitoJUnitRunner.class)
public class HomeworkServiceImplTests {
    @Mock
    private HomeworkRepository mockHomeworkRepository;

    @Mock
    private UserLectureRepository mockUserLectureRepository;

    @Mock
    private UserCourseRepository mockUserCourseRepository;

    @Mock
    private CourseRepository mockCourseRepository;

    @Mock
    private LectureRepository mockLectureRepository;

    @InjectMocks
    private HomeworkServiceImpl homeworkService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Homework homework = new Homework(0, "file.txt", "Student", 1, 1, 0.0, "text", image);
    UserLecture userLecture = new UserLecture(1, null, null, 1, "Student", true, false, true);
    UserLecture nextLecture = new UserLecture(2, 1, null, 1, "Student", true, false, false);
    HomeworkGradeDTO homeworkGradeDTO = new HomeworkGradeDTO(1, "Student", 1, 1, 4.5);
    Category newCategory = new Category(1, "Category Name", image);

    Course courseActive = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, STATUS_ACTIVE, null);

    Lecture lecture = new Lecture(1, "Title", "Description", null, 1, null, null, "video", false);


    @Test(expected = IllegalArgumentException.class)
    public void store_Should_ThrowException_When_UserLectureNotExist() {
        // Arrange
        Mockito.when(mockUserLectureRepository.findByLectureIdAndStudentAndAvailable(homework.getLectureId(), homework.getStudent(), true))
                .thenReturn(null);
        // Act
        homeworkService.store(homework, "Student");

        // Assert
        Mockito.verify(mockHomeworkRepository, Mockito.never()).save(homework);
    }

    @Test(expected = IllegalArgumentException.class)
    public void store_Should_ThrowException_When_HomeworkAlreadySubmitted() {
        // Arrange
        Mockito.when(mockUserLectureRepository.findByLectureIdAndStudentAndAvailable(homework.getLectureId(), homework.getStudent(), true))
                .thenReturn(userLecture);
        Mockito.when(mockHomeworkRepository.findByLectureIdAndStudent(homework.getLectureId(), homework.getStudent()))
                .thenReturn(homework);
        // Act
        homeworkService.store(homework, "Student");

        // Assert
        Mockito.verify(mockHomeworkRepository, Mockito.never()).save(homework);
    }

    @Test
    public void store_Should_saveHomework_When_HomeworkIsSubmitted() {
        // Arrange
        Mockito.when(mockUserLectureRepository.findByLectureIdAndStudentAndAvailable(homework.getLectureId(), homework.getStudent(), true))
                .thenReturn(userLecture);
        Mockito.when(mockHomeworkRepository.findByLectureIdAndStudent(homework.getLectureId(), homework.getStudent()))
                .thenReturn(null);
        // Act
        homeworkService.store(homework, "Student");

        // Assert
        Mockito.verify(mockHomeworkRepository, Mockito.times(1)).save(homework);
        Mockito.verify(mockUserLectureRepository, Mockito.times(1)).save(userLecture);
    }

    @Test
    public void store_Should_SetNextLectureAvailable_When_HomeworkIsSubmitted() {
        // Arrange
        Mockito.when(mockUserLectureRepository.findByLectureIdAndStudentAndAvailable(homework.getLectureId(), homework.getStudent(), true))
                .thenReturn(userLecture);
        userLecture.setNextLectureId(2);
        Mockito.when(mockUserLectureRepository.findByLectureIdAndStudentAndAvailable(userLecture.getNextLectureId(), homework.getStudent(), false))
                .thenReturn(nextLecture);
        Mockito.when(mockHomeworkRepository.findByLectureIdAndStudent(homework.getLectureId(), homework.getStudent()))
                .thenReturn(null);
        // Act
        homeworkService.store(homework, "Student");

        // Assert
        Mockito.verify(mockUserLectureRepository, Mockito.times(1)).save(nextLecture);
    }

    @Test
    public void getUserAverageGradeByCourse_Should_ReturnZero_When_UserGradesNotExist() {
        // Arrange
        Mockito.when(mockHomeworkRepository.getUsersAverageGradeByCourse(1, "Student"))
                .thenReturn(null);

        // Act
        double avgGrade = homeworkService.getUserAverageGradeByCourse(1, "Student", "Student");

        // Assert
        Assert.assertEquals(0.0, avgGrade, 0.001);
    }

    @Test
    public void getUserAverageGradeByCourse_Should_ReturnAvgGrade_When_UserGradesFound() {
        // Arrange
        Mockito.when(mockHomeworkRepository.getUsersAverageGradeByCourse(1, "Student"))
                .thenReturn(3.0);

        // Act
        double avgGrade = homeworkService.getUserAverageGradeByCourse(1, "Student", "Student");

        // Assert
        Assert.assertEquals(3.0, avgGrade, 0.001);
    }

    @Test
    public void getUserGrade_Should_ReturnZero_When_UserHomeworkNotFound() {
        //
        Mockito.when(mockHomeworkRepository.findByLectureIdAndStudent(1, "Student"))
                .thenReturn(null);

        // Act
        double grade = homeworkService.getUserGrade(1, "Student", "Student");

        //
        Assert.assertEquals(0.0, grade, 0.001);
    }

    @Test
    public void getUserGrade_Should_ReturnZero_When_UserHomeworkNotGraded() {
        //
        Mockito.when(mockHomeworkRepository.findByLectureIdAndStudent(1, "Student"))
                .thenReturn(new Homework(1, "file.txt", "Student", 1, 1, null, "text", image));

        // Act
        double grade = homeworkService.getUserGrade(1, "Student", "Student");

        //
        Assert.assertEquals(0.0, grade, 0.001);
    }

    @Test
    public void getUserGrade_Should_ReturnGrade_When_UserHomeworkIsGraded() {
        //
        Mockito.when(mockHomeworkRepository.findByLectureIdAndStudent(1, "Student"))
                .thenReturn(new Homework(1, "file.txt", "Student", 1, 1, 3.5, "text", image));

        // Act
        double grade = homeworkService.getUserGrade(1, "Student", "Student");

        //
        Assert.assertEquals(3.5, grade, 0.001);
    }

    @Test
    public void countByCourseId_Should_ReturnCount_When_MethodInvoked() {
        // Arrange
        Mockito.when(mockHomeworkRepository.countByCourseIdAndGradeIsNull(1)).thenReturn(2);

        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        // Act
        int count = homeworkService.countByCourseId(1, "vasko");

        // Assert
        Assert.assertEquals(2, count);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_HomeworkNotFound() {
        // Arrange

        // Act, Assert
        homeworkService.getById(1, 1, 1, "Student");
    }

    @Test
    public void getById_Should_ReturnHomework_When_HomeworkIsPresent() {
        // Arrange
        Mockito.when(mockHomeworkRepository.findById(1))
                .thenReturn(Optional.of(homework));

        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        // Act
        Homework result = homeworkService.getById(1, 1, 1, "vasko");

        // Assert
        Assert.assertSame(homework, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_LectureIdsNotMatch(){
        // Arrange
        Mockito.when(mockHomeworkRepository.findById(1))
                .thenReturn(Optional.of(homework));

        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        // Act // Assert
        homeworkService.getById(1, 2, 1, "vasko");
    }

    @Test
    public void getByLectureId_Should_ReturnHomeworkList_When_MethodInvoked() {
        // Arrange
        Mockito.when(mockHomeworkRepository.findAllByLectureIdAndGradeIsNull(1))
                .thenReturn(Arrays.asList(homework));

        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(lecture));

        // Act
        List<Homework> result = homeworkService.getByLectureId(1, 1, "vasko");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByLectureId_Should_ThrowException_When_LectureNotBelongToCourse() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(new Lecture(1, "Title", "Description", null, 2, null, null, "video", false)));

        // Act, Assert
        homeworkService.getByLectureId(1, 1, "vasko");
    }

    @Test(expected = IllegalArgumentException.class)
    public void gradeHomework_Should_ThrowException_When_HomeworkNotExist() {
        // Arrange

        // Act
        homeworkService.gradeHomework(homeworkGradeDTO, 1, 1, "Student");

        // Assert
        Mockito.verify(mockHomeworkRepository, Mockito.never()).gradeHomework(homeworkGradeDTO.getId(), homeworkGradeDTO.getGrade());
    }

    @Test
    public void gradeHomework_Should_UpdateGrade_When_HomeworkExist() {
        // Arrange
        homework.setId(1);
        Mockito.when(mockHomeworkRepository.findById(homeworkGradeDTO.getId()))
                .thenReturn(Optional.of(homework));

        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(lecture));

        // Act
        homeworkService.gradeHomework(homeworkGradeDTO, 1, 1, "vasko");

        // Assert
        Mockito.verify(mockHomeworkRepository, Mockito.times(1)).gradeHomework(homeworkGradeDTO.getId(), homeworkGradeDTO.getGrade());
    }

    @Test
    public void gradeHomework_Should_NotSetUseCourseComplete_When_NotAllHomeworkGraded() {
        // Arrange
        homework.setId(1);
        Mockito.when(mockHomeworkRepository.findById(homeworkGradeDTO.getId()))
                .thenReturn(Optional.of(homework));

        Mockito.when(mockHomeworkRepository.countByCourseIdAndStudentAndGradeIsNull(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent()))
                .thenReturn(1);

        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(lecture));

        // Act
        homeworkService.gradeHomework(homeworkGradeDTO, 1, 1, "vasko");

        // Assert
        Mockito.verify(mockHomeworkRepository, Mockito.never()).getUsersAverageGradeByCourse(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent());
        Mockito.verify(mockUserCourseRepository, Mockito.never()).setCompleted(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent());
    }

    @Test
    public void gradeHomework_Should_NotSetUseCourseComplete_When_AverageGradeBelowThree() {
        // Arrange
        homework.setId(1);
        Mockito.when(mockHomeworkRepository.findById(homeworkGradeDTO.getId()))
                .thenReturn(Optional.of(homework));
        Mockito.when(mockHomeworkRepository.countByCourseIdAndStudentAndGradeIsNull(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent()))
                .thenReturn(0);
        Mockito.when(mockHomeworkRepository.getUsersAverageGradeByCourse(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent()))
                .thenReturn(2.5);
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);
        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(lecture));

        // Act
        homeworkService.gradeHomework(homeworkGradeDTO, 1, 1, "vasko");

        // Assert
        Mockito.verify(mockUserCourseRepository, Mockito.never()).setCompleted(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent());
    }

    @Test
    public void gradeHomework_Should_SetUseCourseComplete_When_AverageGradeAtLeastThree() {
        // Arrange
        homework.setId(1);
        Mockito.when(mockHomeworkRepository.findById(homeworkGradeDTO.getId()))
                .thenReturn(Optional.of(homework));
        Mockito.when(mockHomeworkRepository.countByCourseIdAndStudentAndGradeIsNull(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent()))
                .thenReturn(0);
        Mockito.when(mockHomeworkRepository.getUsersAverageGradeByCourse(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent()))
                .thenReturn(3.0);

        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(lecture));

        // Act
        homeworkService.gradeHomework(homeworkGradeDTO, 1, 1, "vasko");

        // Assert
        Mockito.verify(mockUserCourseRepository, Mockito.times(1)).setCompleted(homeworkGradeDTO.getCourseId(), homeworkGradeDTO.getStudent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void gradeHomework_Should_ThrowException_When_UsernamesNotMatches(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(1, STATUS_ACTIVE))
                .thenReturn(courseActive);

        // Act
        homeworkService.gradeHomework(homeworkGradeDTO, 1, 1, "test");

        // Assert
    }

    @Test(expected = IllegalArgumentException.class)
    public void setHomework_Should_ThrowException_When_LectureIdsNotMatch(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(1, STATUS_ACTIVE))
                .thenReturn(courseActive);

        Mockito.when(mockLectureRepository.findById(1))
                .thenReturn(Optional.ofNullable(lecture));

        // Act
        homeworkService.gradeHomework(homeworkGradeDTO, 1, 1, "vasko");

        // Assert
    }
}
