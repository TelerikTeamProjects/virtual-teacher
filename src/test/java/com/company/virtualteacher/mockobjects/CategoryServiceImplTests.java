package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.repositories.CategoryRepository;
import com.company.virtualteacher.services.CategoryServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceImplTests {
    @Mock
    private CategoryRepository mockCategoryRepository;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Category newCategory = new Category(1, "Category Name", image);

    @Test
    public void create_Should_CallRepositorySave_When_CategoryNameIsUnique() {
        // Arrange

        // Act
        categoryService.create(newCategory, "ROLE_TEACHER");

        // Assert
        Mockito.verify(mockCategoryRepository, Mockito.times(1)).save(newCategory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_CategoryNameExists() {
        // Arrange
        Mockito.when(mockCategoryRepository.findCategoryByName(newCategory.getName()))
                .thenReturn(newCategory);

        // Act
        categoryService.create(newCategory, "ROLE_TEACHER");
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_RoleNotTeacher() {
        // Arrange, Act, Assert
        categoryService.create(newCategory, "ROLE_STUDENT");
    }

    @Test
    public void update_Should_CallRepositorySave_When_CategoryExists() {
        // Arrange
        Mockito.when(mockCategoryRepository.findById(newCategory.getId()))
                .thenReturn(Optional.of(newCategory));

        // Act
        categoryService.update(newCategory);

        // Assert
        Mockito.verify(mockCategoryRepository, Mockito.times(1)).save(newCategory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_CategoryNotExists() {
        // Arrange

        // Act
        categoryService.update(newCategory);

        // Assert
        Mockito.verify(mockCategoryRepository, Mockito.never()).save(newCategory);
    }

    @Test
    public void getById_Should_ReturnCategory_When_CategoryWithIdExists(){
        // Arrange
        Mockito.when(mockCategoryRepository.findById(newCategory.getId()))
                .thenReturn(Optional.of(newCategory));

        // Act
        Category result = categoryService.getById(newCategory.getId());

        // Assert
        Assert.assertEquals(1, result.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_CategoryWithIdNotExists(){
        // Arrange

        // Act
        categoryService.getById(newCategory.getId());

        // Assert
        Mockito.verify(mockCategoryRepository, Mockito.never()).findById(newCategory.getId());
    }

    @Test
    public void getCategories_Should_ReturnOrderedCategoryList_When_CategoriesExist(){
        // Arrange
        Category firstCategory = new Category(1, "First Category", image);
        Category secondCategory = new Category(1, "Second Category", image);
        Category thirdCategory = new Category(1, "3 Category", image);

        List<Category> categories = new ArrayList<>();
        categories.add(thirdCategory);
        categories.add(firstCategory);
        categories.add(secondCategory);

        Mockito.when(mockCategoryRepository.findAllByOrderByNameAsc())
                .thenReturn(categories);

        // Act
        List<Category> result = categoryService.getCategories();

        // Assert
        Assert.assertEquals(3, result.size());
    }
}
