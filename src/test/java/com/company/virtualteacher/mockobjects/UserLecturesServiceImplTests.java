package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.UserLecture;
import com.company.virtualteacher.repositories.UserLectureRepository;
import com.company.virtualteacher.services.UserLecturesServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserLecturesServiceImplTests {
    @Mock
    private UserLectureRepository mockUserLectureRepository;

    @InjectMocks
    private UserLecturesServiceImpl userLecturesService;

    UserLecture userLecture = new UserLecture(1, null, null, 1,
            "didig", false, false, false);

    @Test
    public void getUserLectures_Should_ReturnUserLecturesByCourseList_When_CourseAndStudentAreValid(){
        // Arrange
        List<UserLecture> userLectureList = new ArrayList<>();
        userLectureList.add(userLecture);

        Mockito.when(mockUserLectureRepository
                .findAllByCourseIdAndStudentOrderByLectureId(1, userLecture.getStudent()))
                .thenReturn(userLectureList);

        // Act
        List<UserLecture> result = userLecturesService.getUserLectures(1, userLecture.getStudent(), "didig");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserLectures_Should_ThrowException_When_UserNotMatch(){
        // Arrange, Act, Assert
        userLecturesService.getUserLectures(1, userLecture.getStudent(), "vasko");
    }

    @Test
    public void isLectureAvailable_Should_ReturnLecture_When_StudentIsAuthorisedToWatchIt(){
        // Arrange
        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(1, "didig", true))
                .thenReturn(userLecture);

        // Act
        Boolean result = userLecturesService.isLectureAvailable(1, "didig");

        // Assert
        Assert.assertEquals(result, true);
    }

    @Test
    public void setLectureAvailable_Should_SetLectureAccessible_When_StudentIsAuthorised(){
        // Arrange
        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(1, "didig", false))
                .thenReturn(userLecture);

        // Act
        userLecturesService.setLectureAvailable(1, "didig", "didig");

        // Assert
        Mockito.verify(mockUserLectureRepository, Mockito.times(1)).save(userLecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setLectureAvailable_Should_ThrowException_When_UserLectureNotFound(){
        // Arrange, Act
        userLecturesService.setLectureAvailable(1, "didig", "didig");

        // Assert
        Mockito.verify(mockUserLectureRepository, Mockito.never()).save(userLecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setLectureAvailable_Should_ThrowException_When_PrevLecturePresentButNotAvailable(){
        // Arrange
        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(1, "didig", false))
                .thenReturn(new UserLecture(1, 2, null, 1,
                        "didig", false, false, false));
        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(2, "didig", true))
                .thenReturn(new UserLecture(2, null, 1, 1,
                        "didig", false, false, false));
        // Act
        userLecturesService.setLectureAvailable(1, "didig", "didig");

        // Assert
        Mockito.verify(mockUserLectureRepository, Mockito.never()).save(userLecture);
    }


    @Test
    public void getByLectureIdAndStudent_Should_ReturnUserLecture_When_StudentIsAuthorised(){
        // Arrange
        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(1, "didig", true))
                .thenReturn(userLecture);

        // Act
        UserLecture result = userLecturesService.getByLectureIdAndStudent(1, "didig", "didig");

        // Assert
        Assert.assertEquals(result, userLecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByLectureIdAndStudent_Should_ThrowException_When_StudentIsNotAuthorised(){
        // Arrange

        // Act
        userLecturesService.getByLectureIdAndStudent(1, "didig", "didig");

        // Assert
        Mockito.verify(mockUserLectureRepository, Mockito.never()).save(userLecture);
    }

    @Test
    public void setVideoWatched_Should_SetVideoWatched_When_StudentIsAuthorised(){
        // Arrange
        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(1, "didig", true))
                .thenReturn(userLecture);

        // Act
        userLecturesService.setVideoWatched(1, "didig", "didig");

        // Assert
        Mockito.verify(mockUserLectureRepository, Mockito.times(1)).save(userLecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setVideoWatched_Should_ThrowException_When_StudentIsNotAuthorised(){
        // Arrange

        // Act
        userLecturesService.setVideoWatched(1, "didig", "didig");

        // Assert
        Mockito.verify(mockUserLectureRepository, Mockito.never()).save(userLecture);
    }

    @Test
    public void getProgressByCourseId_Should_ReturnCourseProgrees_When_CourseAndStudentAreValid(){
        // Arrange
        userLecture = new UserLecture(1, null, null, 1,
                "didig", true, true, true);

        UserLecture userLectureTwo = new UserLecture(2, null, null, 1,
                "didig", true, false, true);

        UserLecture userLectureThree = new UserLecture(3, null, null, 1,
                "didig", false, false, false);

        List<UserLecture> userLectureList = new ArrayList<>();
        userLectureList.add(userLecture);
        userLectureList.add(userLectureTwo);
        userLectureList.add(userLectureThree);

        Mockito.when(mockUserLectureRepository.findAllByCourseIdAndStudentOrderByLectureId(userLecture.getCourseId(),
                userLecture.getStudent()))
                .thenReturn(userLectureList);

        // Act
        int result = userLecturesService
                .getProgressByCourseId(userLecture.getCourseId(), userLecture.getStudent(), "didig");

        // Assert
        Assert.assertEquals(result, 50);
    }

    @Test
    public void getProgressByCourseId_Should_ReturnCourseProgreesZero_When_CourseAndStudentAreValid(){
        // Arrange
        List<UserLecture> userLectureList = new ArrayList<>();

        Mockito.when(mockUserLectureRepository.findAllByCourseIdAndStudentOrderByLectureId(1, "didig"))
                .thenReturn(userLectureList);

        // Act
        int result = userLecturesService
                .getProgressByCourseId(1, "didig", "didig");

        // Assert
        Assert.assertEquals(result, 0);
    }
}
