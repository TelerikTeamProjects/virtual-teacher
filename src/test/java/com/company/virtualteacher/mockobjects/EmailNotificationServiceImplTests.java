package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.User;
import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.EmailNotificationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@RunWith(MockitoJUnitRunner.class)
public class EmailNotificationServiceImplTests {
    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private JavaMailSender mockJavaMailSender;

    @InjectMocks
    private EmailNotificationServiceImpl emailNotificationService;

    @Test
    public void sendEmail_Should_SendEmail_When_UserEmailExists() throws MessagingException {
        // Arrange
        User user = new User(1, "username1", "user@email.com", "First Name",
                "Middle Name",
                "Last Name", true,  null);
        MimeMessage msg = new JavaMailSenderImpl().createMimeMessage();

        Mockito.when(mockJavaMailSender.createMimeMessage())
                .thenReturn(msg);

        Mockito.when(mockUserRepository.findUserByUsername(user.getUsername()))
                .thenReturn(user);

        // Act
        emailNotificationService.sendEmail(user.getUsername());

        // Assert
        Mockito.verify(mockJavaMailSender, Mockito.times(1)).send(msg);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sendEmail_Should_ThrowException_When_UserEmailNotExists() throws MessagingException {
        // Arrange
        User user = new User(1, "username1", null, "First Name", "Middle Name",
                "Last Name", true,  null);

        Mockito.when(mockJavaMailSender.createMimeMessage())
                .thenReturn(new JavaMailSenderImpl().createMimeMessage());

        Mockito.when(mockUserRepository.findUserByUsername(user.getUsername()))
                .thenReturn(user);

        // Act
        emailNotificationService.sendEmail(user.getUsername());
    }
}
