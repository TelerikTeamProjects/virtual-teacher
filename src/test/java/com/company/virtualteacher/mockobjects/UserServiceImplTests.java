package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.User;
import com.company.virtualteacher.repositories.PendingApprovalRepository;
import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {
    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private UserDetailsManager mockUserDetailsManager;

    @Mock
    private PendingApprovalRepository mockPendingApprovalsRepository;

    @InjectMocks
    private UserServiceImpl userService;

    String resume = "Motivation";
    List<GrantedAuthority> authorities;

    @Test
    public void create_Should_CallUserDetailsManagerAndUserRepositorySave_When_UserNameIsUnique() {
        // Arrange
        String userEmail = "user@email.com";
        authorities = AuthorityUtils.createAuthorityList("ROLE_STUDENT");
        org.springframework.security.core.userdetails.User newUser = new
                org.springframework.security.core.userdetails.User("username", "123345678", authorities);

        User createdUser = new User(1, "username", "user@email.com", "First Name", "Middle Name",
                "Last Name", true,  null);

        Mockito.when(mockUserDetailsManager.userExists(newUser.getUsername()))
                .thenReturn(false);

        Mockito.when(mockUserRepository.findUserByUsername(newUser.getUsername()))
                .thenReturn(createdUser);

        // Act
        userService.create(newUser, resume, userEmail);

        // Assert
        Mockito.verify(mockUserDetailsManager, Mockito.times(1)).createUser(newUser);
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(createdUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_UserNameIsNotUnique() {
        // Arrange
        String userEmail = "user@user.com";
        authorities = AuthorityUtils.createAuthorityList("ROLE_STUDENT");
        org.springframework.security.core.userdetails.User newUser = new
                org.springframework.security.core.userdetails.User("username", "123345678", authorities);

        Mockito.when(mockUserDetailsManager.userExists(newUser.getUsername()))
                .thenReturn(true);

        // Act
        userService.create(newUser, resume, userEmail);

        // Assert
        Mockito.verify(mockUserDetailsManager, Mockito.times(1)).updateUser(newUser);
    }

    @Test
    public void changeRole_Should_ChangeExistingRole_When_UserExists(){
        // Arrange
        authorities = AuthorityUtils.createAuthorityList("ROLE_TEACHER");
        org.springframework.security.core.userdetails.User user =
                new org.springframework.security.core.userdetails.User("didig", "1234", authorities);


        // Act
        userService.changeRole(user);

        // Assert
        Mockito.verify(mockUserDetailsManager, Mockito.times(1)).updateUser(user);
    }

    @Test
    public void getDetails_Should_ReturnUser_When_UsernameIsValid(){
        // Arrange
        Mockito.when(mockUserRepository.findUserByUsername("vasko"))
                .thenReturn(new User(1, "vasko", "user@email.com", "First Name", "Middle Name",
                        "Last Name", true,  null));

        // Act
        User result = userService.getDetails("vasko");

        // Assert
        Assert.assertEquals(result.getUsername(), "vasko");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDetails_Should_ThrowException_When_UsernameNotValid(){
        // Arrange & // Act
        userService.getDetails("vasko");

        // Assert
    }

    @Test
    public void update_Should_UpdateUserDetails_When_UserExists(){
        // Arrange
        User existingUser = new User(1, "vasko", "user@email.com", "First Name", "Middle Name",
                "Last Name", true,  null);
        Mockito.when(mockUserRepository.findUserByUsername(existingUser.getUsername()))
                .thenReturn(existingUser);

        // Act
        userService.update(existingUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateDetails(existingUser.getUsername(), existingUser.getEmail(), existingUser.getFirstName(),
                        existingUser.getMiddleName(), existingUser.getLastName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_UserNotExists(){
        // Arrange
        User existingUser = new User(1, "vasko", "user@email.com", "First Name", "Middle Name",
                "Last Name", true,  null);

        // Act
        userService.update(existingUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.never())
                .updateDetails(existingUser.getUsername(), existingUser.getEmail(), existingUser.getFirstName(),
                        existingUser.getMiddleName(), existingUser.getLastName());
    }

    @Test
    public void delete_Should_DeleteUser_When_UserExists(){
        // Arrange
        List<GrantedAuthority> authorities = AuthorityUtils.NO_AUTHORITIES;

        org.springframework.security.core.userdetails.UserDetails newUser =
                new org.springframework.security.core.userdetails.User("vasko", "", false, true, true, true, authorities);

        User existingUser = new User(1, "vasko", "user@email.com", "First Name", "Middle Name",
                "Last Name", true,  null);
        Mockito.when(mockUserRepository.findUserByUsername(existingUser.getUsername()))
                .thenReturn(existingUser);

        // Act
        userService.delete(newUser);

        // Assert
        Mockito.verify(mockUserDetailsManager, Mockito.times(1)).updateUser(newUser);
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(existingUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_UserNotExists(){
        // Arrange
        List<GrantedAuthority> authorities = AuthorityUtils.NO_AUTHORITIES;

        org.springframework.security.core.userdetails.UserDetails newUser =
                new org.springframework.security.core.userdetails.User("vasko", "", false, true, true, true, authorities);

        User existingUser = new User(1, "vasko", "user@email.com", "First Name", "Middle Name",
                "Last Name", true,  null);

        // Act
        userService.delete(newUser);

        // Assert
        Mockito.verify(mockUserDetailsManager, Mockito.never()).updateUser(newUser);
        Mockito.verify(mockUserRepository, Mockito.never()).save(existingUser);
    }
}
