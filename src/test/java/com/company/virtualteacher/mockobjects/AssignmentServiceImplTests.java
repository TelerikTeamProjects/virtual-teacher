package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.*;
import com.company.virtualteacher.repositories.AssignmentRepository;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.repositories.UserLectureRepository;
import com.company.virtualteacher.services.AssignmentServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static com.company.virtualteacher.services.CourseStatus.STATUS_ACTIVE;
import static com.company.virtualteacher.services.CourseStatus.STATUS_DRAFT;

@RunWith(MockitoJUnitRunner.class)
public class AssignmentServiceImplTests {
    @Mock
    private AssignmentRepository mockAssignmentRepository;

    @Mock
    private LectureRepository mockLectureRepository;

    @Mock
    private CourseRepository mockCourseRepository;

    @Mock
    private UserLectureRepository mockUserLectureRepository;

    @InjectMocks
    private AssignmentServiceImpl assignmentService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Category newCategory = new Category(1, "Category Name", image);

    Course courseDraft = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, STATUS_DRAFT, null);
    Course courseActive = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, STATUS_ACTIVE, null);
    Lecture lecture;
    Assignment assignment;

    @Test
    public void store_Should_StoreAssigmentToCourse_When_CourseIsInStatusDraft() {
        // Arrange
       lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);

        assignment = new Assignment(lecture.getId(), "assignment", "type", image);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(java.util.Optional.of(lecture));

        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(),
                STATUS_DRAFT))
                .thenReturn(courseDraft);

        // Act
        Assignment result = assignmentService.store(assignment, "vasko");

        // Assert
        Assert.assertEquals(result, assignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void store_Should_ThrowException_When_LectureNotExists() {
        // Arrange
        lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);

        assignment = new Assignment(lecture.getId(), "assignment", "type", image);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(java.util.Optional.of(lecture));

        // Act
        assignmentService.store(assignment, "didig");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void store_Should_ThrowException_When_LectureIsNotInStatusDraft() {
        // Arrange
        lecture = new Lecture(1, "Title", "Description", 1, courseActive.getId(),
                null, null, null, true);

        assignment = new Assignment(lecture.getId(), "assignment", "type", image);

        // Act
        assignmentService.store(assignment, "didig");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test
    public void get_Should_ReturnAssigment_When_AssignmentIdIsValid() {
        // Arrange
        assignment = new Assignment(1,1, "assignment", "type", image);

        lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);

        Mockito.when(mockAssignmentRepository.findById(assignment.getId()))
                .thenReturn(Optional.of(assignment));

        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(lecture.getId(), "didig", true))
                .thenReturn(new UserLecture(1, 1, 2, 1, "didig",
                        true, false, true));

        // Act
        Assignment result = assignmentService.get(assignment.getId(),1, "didig");

        // Assert
        Assert.assertEquals(result, assignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Should_ThrowException_When_AssignmentNotMatchLecture() {
        // Arrange
        assignment = new Assignment(1,2, "assignment", "type", image);

        Mockito.when(mockAssignmentRepository.findById(assignment.getId()))
                .thenReturn(Optional.of(assignment));

        // Act, Assert
        assignmentService.get(assignment.getId(),1, "didig");
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Should_ThrowException_When_LectureNotAvailable() {
        // Arrange
        assignment = new Assignment(1,1, "assignment", "type", image);

        Mockito.when(mockAssignmentRepository.findById(assignment.getId()))
                .thenReturn(Optional.of(assignment));

        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(1, "didig", true))
                .thenReturn(null);

        // Act, Assert
        assignmentService.get(assignment.getId(),1, "didig");
    }

    @Test
    public void getFileName_Should_ReturnFileName_When_FileExists() {
        // Arrange
        assignment = new Assignment(1,1, "File Name", "type", image);

        lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);

        Mockito.when(mockAssignmentRepository.findById(assignment.getId()))
                .thenReturn(Optional.ofNullable(assignment));

        Mockito.when(mockLectureRepository.findById(1))
                .thenReturn(Optional.ofNullable(lecture));

        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(),
                STATUS_DRAFT))
                .thenReturn(courseDraft);

        // Act
        String result = assignmentService.getFileName(1,1, "vasko", "ROLE_TEACHER");

        // Assert
        Assert.assertEquals(result, assignment.getFileName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFileName_Should_ThrowException_When_UserNotCourseTeacher() {
        // Arrange
        assignment = new Assignment(1,1, "File Name", "type", image);

        lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);

        Mockito.when(mockAssignmentRepository.findById(assignment.getId()))
                .thenReturn(Optional.ofNullable(assignment));

        Mockito.when(mockLectureRepository.findById(1))
                .thenReturn(Optional.ofNullable(lecture));

        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(),
                STATUS_DRAFT))
                .thenReturn(courseDraft);

        // Act, Assert
        assignmentService.getFileName(1,1, "didig", "ROLE_TEACHER");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFileName_Should_ThrowException_When_AssignmentNotMatchLecture() {
        // Arrange
        assignment = new Assignment(1,1, "File Name", "type", image);

        lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);

        Mockito.when(mockAssignmentRepository.findById(assignment.getId()))
                .thenReturn(Optional.ofNullable(assignment));

        // Act, Assert
        assignmentService.getFileName(1,2, "vasko", "ROLE_TEACHER");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFileName_Should_ThrowException_When_StudentLectureNotAvailable() {
        // Arrange
        assignment = new Assignment(1,1, "File Name", "type", image);

        lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);

        Mockito.when(mockAssignmentRepository.findById(assignment.getId()))
                .thenReturn(Optional.ofNullable(assignment));

        Mockito.when(mockUserLectureRepository
                .findByLectureIdAndStudentAndAvailable(1, "vasko", true))
                .thenReturn(null);

        // Act
        String result = assignmentService.getFileName(1,1, "vasko", "ROLE_STUDENT");

        // Assert
        Assert.assertEquals(result, assignment.getFileName());
    }

    @Test
    public void store_Should_RestoreAssigmentToCourse_When_CourseIsInStatusDraft() {
        // Arrange
        lecture = new Lecture(1, "Title", "Description", 1, courseDraft.getId(),
                null, null, null, true);
        assignment = new Assignment(lecture.getId(), "assignment", "type", image);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(java.util.Optional.of(lecture));

        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        Mockito.when(mockAssignmentRepository.findByLectureId(lecture.getId()))
                .thenReturn(assignment);

        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(),
                STATUS_DRAFT))
                .thenReturn(courseDraft);

        // Act
        Assignment result = assignmentService.store(assignment, "vasko");

        // Assert
        Assert.assertEquals(result, assignment);
    }
}
