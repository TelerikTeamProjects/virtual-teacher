package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.PendingApprovals;
import com.company.virtualteacher.repositories.PendingApprovalRepository;
import com.company.virtualteacher.services.PendingApprovalServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class PendingApprovalServiceImplTests {
    @Mock
    private PendingApprovalRepository mockPendingApprovalsRepository;

    @InjectMocks
    private PendingApprovalServiceImpl pendingApprovalService;

    PendingApprovals pendingApproval = new PendingApprovals("vasko", "The best teacher ever :)");

    @Test
    public void approve_Should_DeletePandingApprovals_When_PendingApprovalsExist(){
        // Arrange
        List<PendingApprovals> pendingApprovalsList = new ArrayList<>();
        pendingApprovalsList.add(pendingApproval);

        Mockito.when(mockPendingApprovalsRepository.findAllByUsername(pendingApproval.getUsername()))
                .thenReturn(pendingApprovalsList);

        // Act
        pendingApprovalService.approve(pendingApproval.getUsername());

        // Assert
        Mockito.verify(mockPendingApprovalsRepository, Mockito.times(1)).delete(pendingApproval);
    }

    @Test(expected = IllegalArgumentException.class)
    public void approve_Should_ThrowException_When_PendingApprovalsNotExist(){
        // Arrange

        // Act
        pendingApprovalService.approve("didig");

        // Assert
        Mockito.verify(mockPendingApprovalsRepository, Mockito.never()).delete(pendingApproval);
    }

    @Test
    public void getPendingApprovals_Should_ReturnPageWithPendingApprovals_When_MethodIsInvoked(){
        // Arrange
        List<PendingApprovals> pendingApprovals = new ArrayList<>();
        pendingApprovals.add(pendingApproval);

        Page<PendingApprovals> page = new PageImpl<>(pendingApprovals);

        Mockito.when(mockPendingApprovalsRepository.findAllByUsernameContaining(any(),
                any()))
                .thenReturn(page);

        // Act
        Page<PendingApprovals> result = pendingApprovalService.getPendingApprovals(any(), any());

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }
}
