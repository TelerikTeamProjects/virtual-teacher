# Java Team Project

## [Project Description]()
Develop VIRTUAL TEACHER web application, an online platform for tutoring. Users will be able to register as either a teacher or a student.  
Students should be able to enroll for video courses, watch the available the video lectures for them and after successful completion rate them. After finishing each video within a course, students will be asked to submit an assignment, which will be graded by the teacher.  
Teachers should be able to create courses and upload video lectures with assignments. Each assignment will be graded individually after submission from the students.  Users can become teachers only after approval from administrator.  

## [Project Requirements]()

### [Web application]() 
#### **Public Section**
The public part of your projects should be **visible without authentication**. This includes the application landing page, the user login, a student and a teacher registration forms, as well as a catalog with the available courses. The catalogs you can filter by name, topic and teacher and order by name and rating. Users who are not logged in, cannot enroll for a course. 

#### **Private Part (All users)**
**Registered users** should have private section (profile) in the web application accessible after **successful login**. 

Users should be able to: 
 - view and edit their profile (name, email and picture) 
 - change their password 
 - enroll for courses  
 - see their enrolled and completed courses 
 
Courses: 
 - The videos in a course will be accessible only after enrollment. 
 - The application will keep track of the student’s progress through the course.  
 - The assignments will unlock after a student finishes a lecture. 
 - Students are required to submit their work as a file (txt, doc, java, etc.).  
 - Students will see their grades for the assignment and also their average grade for the course.  
 - After receiving a grade for their last assignment for the course, if their average grade is above the passing grade, they can leave a rating for the course. 
 
#### **Private Part (Teachers only)**
Teacher users have access to all the functionality that students do. However, when they are approved by an administrator, a course administration page is accessible to them. On it they can create, edit and delete **courses**. 

Each **course** should have: 
 - title 
 - topic (writing, history, programming etc.) 
 - description  
 - list of **lectures**.  
 
Each **lecture** should have: 
 - title 
 - description and/or notes 
 - video 
 - assignment 
 
Once a course is set up, there should be a button to submit it. After the submission, the course become available for enrollment and the teachers now have the option to download and grade the assignments. 
 
(Optional) A comment page for each course, where students can comment on the lectures and ask questions. 

#### **Administration Part**
Administrator users can’t be registered though the ordinary process.  They can delete courses and users, create other administrators and approve teachers. 
 
 Follow us on our Trello Kanban Board [https://<i></i>trello.com/b/8BCVogtD/virtual-teacher]() :eyes: :wink: :four_leaf_clover: